# Translation of ktp-common-internals.desktop to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2012, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: desktop files\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-05-01 14:04+0200\n"
"PO-Revision-Date: 2014-09-30 11:06+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:3
msgctxt "Name"
msgid "Instant messaging"
msgstr "Lynmelding"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:48
msgctxt "Comment"
msgid "Instant messaging"
msgstr "Lynmelding"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:95
msgctxt "Name"
msgid "Group"
msgstr "Gruppe"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:142
msgctxt "Comment"
msgid "The group where the contact resides"
msgstr "Gruppa der kontakten finnes"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:188
msgctxt "Name"
msgid "Contact"
msgstr "Kontakt"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:235
msgctxt "Comment"
msgid "The specified contact"
msgstr "Den oppgitte kontakten"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:282
msgctxt "Name"
msgid "Class"
msgstr "Klasse"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:329
msgctxt "Comment"
msgid "The message class"
msgstr "Meldingsklasse"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:379
msgctxt "Name"
msgid "Instant messaging error"
msgstr "Lynmeldingsfeil"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:423
msgctxt "Comment"
msgid "An error occurred within instant messaging"
msgstr "Det oppsto en feil i lynmelding"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:472
msgctxt "Name"
msgid "Instant messaging info message"
msgstr "Informasjonsmelding fra lynmelding"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:515
msgctxt "Comment"
msgid "A general info message from instant messaging"
msgstr "En generell informasjonsmelding fra lynmeldingssystemet"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:565
msgctxt "Name"
msgid "Incoming Message"
msgstr "Innkommende melding"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:612
msgctxt "Comment"
msgid "An incoming message has been received"
msgstr "En innkommende melding er mottatt"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:664
msgctxt "Name"
msgid "Incoming Message in Active Chat"
msgstr "Innkommende melding i aktiv prat"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:709
msgctxt "Comment"
msgid "An incoming message in the active chat window has been received"
msgstr "Det er mottatt en innkommende melding i det aktive pratevinduet"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:759
msgctxt "Name"
msgid "Incoming Message in Group Chat"
msgstr "Innkommende melding i gruppeprat"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:799
msgctxt "Comment"
msgid "An incoming message has been received in a group chat"
msgstr "En innkommende melding er mottatt i en gruppeprat"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:845
msgctxt "Name"
msgid "Incoming Message in Active Group Chat"
msgstr "Innkommende melding i aktiv gruppeprat"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:884
msgctxt "Comment"
msgid "An incoming message in the active group chat window has been received"
msgstr "Det er mottatt en innkommende melding i det aktive gruppe-pratevinduet"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:930
msgctxt "Name"
msgid "Outgoing Message"
msgstr "Utgående melding"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:975
msgctxt "Comment"
msgid "An outgoing message has been sent"
msgstr "En utgående melding er sendt"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1026
msgctxt "Name"
msgid "Highlight in Group Chat"
msgstr "Framhev i gruppeprat"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1065
msgctxt "Comment"
msgid "A highlighted message has been received in a group chat"
msgstr "En framhevet melding er mottatt i en gruppeprat"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1109
msgctxt "Name"
msgid "Highlight in Active Group Chat"
msgstr "Framhev i aktiv gruppeprat"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1148
msgctxt "Comment"
msgid "A highlighted message has been received in the active group chat window"
msgstr "En framhevet melding er mottatt i det aktive gruppeprat-vinduet"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1192
msgctxt "Name"
msgid "Message Dropped"
msgstr "Melding fjernet"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1236
msgctxt "Comment"
msgid "A message was filtered by the Privacy Plugin"
msgstr "En melding ble filtrert bort av programtillegget for personvern"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1283
msgctxt "Name"
msgid "Service Message"
msgstr "Tjenestemelding"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1329
msgctxt "Comment"
msgid "A service message has been received (e.g. authorization request)"
msgstr "En tjenestemelding er mottatt (f.eks. forespørsel om autorisering)"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1380
msgctxt "Name"
msgid "New incoming chat"
msgstr "Ny innkommende prat"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1423
msgctxt "Comment"
msgid "A new instant message has been received"
msgstr "Det er mottatt en ny lynmelding"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1471
msgctxt "Name"
msgid "Incoming file transfer"
msgstr "Innkommende filoverføring"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1515
msgctxt "Comment"
msgid "An incoming file transfer request has been received"
msgstr "Det er mottatt en forespørsel om innkommende filoverføring"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1563
msgctxt "Name"
msgid "Incoming call"
msgstr "Innkommende anrop"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1608
msgctxt "Comment"
msgid "An incoming audio/video call request has been received"
msgstr "Det er mottatt en forespørsel om innkommende audio/video-samtale"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1659
msgctxt "Name"
msgid "Chatstyle Install Request"
msgstr "Chatstyle installasjonsforespørsel"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1707
msgctxt "Name"
msgid "Chatstyle Installed Successfully"
msgstr "Vellykket installert Chatstyle"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1755
msgctxt "Name"
msgid "Chatstyle Installation Failed"
msgstr "Chatstyle-installasjon mislyktes"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1802
msgctxt "Name"
msgid "Emoticonset Install Request"
msgstr "Emoticonset installasjonsforespørsel"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1849
msgctxt "Name"
msgid "Emoticonset Installed Successfully"
msgstr "Vellykket installert Emoticonset"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1897
msgctxt "Name"
msgid "Emoticonset Installation failed"
msgstr "Emoticonset-installasjon mislyktes"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1943
msgctxt "Name"
msgid "Contact status changed"
msgstr "Kontaktstatus endret"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:1986
msgctxt "Comment"
msgid "A contact has changed their status"
msgstr "En kontakt har endret sin status"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:2033
msgctxt "Name"
msgid "File renamed for sending with Google Talk"
msgstr "Fila har fått nytt navn for sending med Google Talk"

#. +> trunk5 stable5
#: data/ktelepathy.notifyrc:2071
msgctxt "Comment"
msgid "An outgoing file with forbidden extension .exe or .ini has been renamed"
msgstr "En utgående fil med forbudt etternavn .exe eller .ini har fått nytt navn"

#. +> trunk5 stable5
#: KTp/Logger/ktp_logger_plugin.desktop:4
msgctxt "Comment"
msgid "KTp Logger Plugin"
msgstr "Programtillegg for KTp-logger"

#. +> trunk5 stable5
#: KTp/Logger/plugins/tplogger/ktploggerplugin_tplogger.desktop.cmake:2
msgctxt "Name"
msgid "Telepathy Logger plugin"
msgstr "Telepathy logger-modul"

#. +> trunk5 stable5
#: KTp/Logger/plugins/tplogger/ktploggerplugin_tplogger.desktop.cmake:40
msgctxt "Comment"
msgid "Provides integration with Telepathy Logger"
msgstr "Skaffer integrering med Telepathy logger"

#~ msgctxt "Comment"
#~ msgid "A highlighted message has been received"
#~ msgstr "En framhevet melding er mottatt"

#~ msgctxt "Name"
#~ msgid "Highlight"
#~ msgstr "Framhev"

#~ msgctxt "Name"
#~ msgid "Buzz/Nudge"
#~ msgstr "Ring/dytt"

#~ msgctxt "Comment"
#~ msgid "A contact has sent you a buzz/nudge"
#~ msgstr "En kontakt har sendt deg en ring/dytt"

#~ msgctxt "Name"
#~ msgid "Nepomuk Telepathy Service"
#~ msgstr "Nepomuk Telepathy-tjeneste"

#~ msgctxt "Comment"
#~ msgid "A Nepomuk service that imports Telepathy data"
#~ msgstr "En Nepomuk-tjeneste som importerer Telepathy-data"

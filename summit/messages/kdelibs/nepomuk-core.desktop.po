# Translation of nepomuk-core.desktop to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2011, 2012, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: desktop files\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-04-04 10:07+0200\n"
"PO-Revision-Date: 2014-04-25 15:56+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk stable
#: baloo/nepomukbaloomigrator.desktop:2
msgctxt "Name"
msgid "nepomukbaloomigrator"
msgstr "nepomukbaloomigrator"

#. +> trunk stable
#: baloo/nepomukbaloomigrator.desktop:43
msgctxt "Comment"
msgid "Tool to migrate the old Nepomuk based data to Baloo."
msgstr "Verktøy for å migrere gamle Nepomuk-baserte data til Baloo"

#. +> trunk stable
#: cleaner/nepomukcleaner.desktop:2
msgctxt "Name"
msgid "Nepomuk Cleaner"
msgstr "Nepomuk-renser"

#. +> trunk stable
#: cleaner/nepomukcleaningjob.desktop:4
msgctxt "Comment"
msgid "Nepomuk Cleaning Job"
msgstr "Nepomuk-rensejobb"

#. +> trunk stable
#: libnepomukcore/service/nepomukservice.desktop:4
msgctxt "Comment"
msgid "Nepomuk Service"
msgstr "Nepomuk-tjeneste"

#. +> trunk stable
#: libnepomukcore/service/nepomukservice2.desktop:4
msgctxt "Comment"
msgid "Nepomuk Service Version 2"
msgstr "Nepomuk-tjeneste versjon 2"

#. +> trunk stable
#: server/nepomukserver.desktop:8
msgctxt "Name"
msgid "Nepomuk Server"
msgstr "Nepomuk-tjener"

#. +> trunk stable
#: server/nepomukserver.desktop:57
msgctxt "Comment"
msgid "The Nepomuk Server providing Storage services and strigi controlling"
msgstr "Nepomuk-tjeneren som skaffer lagringstjenester og strigi-styring"

#. +> trunk stable
#: services/fileindexer/indexer/mobipocket/nepomukmobiextractor.desktop:5
msgctxt "Name"
msgid "Nepomuk Mobi Extractor"
msgstr "Nepomuk Mobi-uttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/mobipocket/nepomukmobiextractor.desktop:46
msgctxt "Comment"
msgid "Nepomuk File extractor for MobiPocket Files"
msgstr "Nepomuk filuttrekker for MobiPocket-filer"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukepubextractor.desktop:5
msgctxt "Name"
msgid "Nepomuk EPub Extractor"
msgstr "Nepomuk EPub-uttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukepubextractor.desktop:45
msgctxt "Comment"
msgid "Nepomuk File extractor for EPub Files"
msgstr "Nepomuk filuttrekker for EPub-filer"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukexiv2extractor.desktop:5
msgctxt "Name"
msgid "Nepomuk Exiv2 Extractor"
msgstr "Nepomuk Exiv2-uttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukexiv2extractor.desktop:49
msgctxt "Comment"
msgid "Nepomuk File extractor for Image files"
msgstr "Nepomuk filuttrekker for bildefiler"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukextractor.desktop:4
msgctxt "Comment"
msgid "Nepomuk File Extractor"
msgstr "Nepomuk filuttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukffmpegextractor.desktop:5
msgctxt "Name"
msgid "Nepomuk FFmpeg Extractor"
msgstr "Nepomuk FFmpeg-uttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukffmpegextractor.desktop:49
msgctxt "Comment"
msgid "Nepomuk File extractor for Video files"
msgstr "Nepomuk filuttrekker for videofiler"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukodfextractor.desktop:5
msgctxt "Name"
msgid "Nepomuk Odf Extractor"
msgstr "Nepomuk Odf-uttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukodfextractor.desktop:45
msgctxt "Comment"
msgid "Nepomuk File extractor for ODF files"
msgstr "Nepomuk filuttrekker for ODF-filer"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukoffice2007extractor.desktop:5
msgctxt "Name"
msgid "Nepomuk Office2007 Extractor"
msgstr "Nepomuk Office2007-uttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukoffice2007extractor.desktop:45
msgctxt "Comment"
msgid "Nepomuk File extractor for Office2007 files"
msgstr "Nepomuk filuttrekker for Office2007-filer"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukofficeextractor.desktop:5
msgctxt "Name"
msgid "Nepomuk Office Extractor"
msgstr "Nepomuk Office-uttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukofficeextractor.desktop:44
msgctxt "Comment"
msgid "Nepomuk File extractor for DOC, XLS and PPT files"
msgstr "Nepomuk filuttrekker for DOC, XLS og PPT-filer"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukplaintextextractor.desktop:5
msgctxt "Name"
msgid "Nepomuk Plain Text Extractor"
msgstr "Nepomuk filuttrekker for ren tekst"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukplaintextextractor.desktop:49
msgctxt "Comment"
msgid "Nepomuk File extractor for text files"
msgstr "Nepomuk filuttrekker for tekstfiler"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukpopplerextractor.desktop:5
msgctxt "Name"
msgid "Nepomuk Poppler Extractor"
msgstr "Nepomuk Poppler-uttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/nepomukpopplerextractor.desktop:49
msgctxt "Comment"
msgid "Nepomuk File extractor for PDF files"
msgstr "Nepomuk filuttrekker for PDF-filer"

#. +> trunk stable
#: services/fileindexer/indexer/nepomuktaglibextractor.desktop:5
msgctxt "Name"
msgid "Nepomuk TagLib Extractor"
msgstr "Nepomuk TagLib-uttrekker"

#. +> trunk stable
#: services/fileindexer/indexer/nepomuktaglibextractor.desktop:49
msgctxt "Comment"
msgid "Nepomuk File extractor for Music Files"
msgstr "Nepomuk filuttrekker for musikkfiler"

#. +> trunk stable
#: services/fileindexer/nepomukfileindexer.desktop:7
msgctxt "Name"
msgid "File Indexer Service"
msgstr "Tjeneste for filindeksering"

#. +> trunk stable
#: services/fileindexer/nepomukfileindexer.desktop:56
msgctxt "Comment"
msgid "Nepomuk Service which indexes files on the desktop"
msgstr "Nepomuk tjeneste som indekserer filene på skrivebordet"

#. +> trunk stable
#: services/fileindexer/nepomukfileindexer.notifyrc:3
msgctxt "Comment"
msgid "Desktop Search"
msgstr "Skrivebordssøk"

#. +> trunk stable
#: services/fileindexer/nepomukfileindexer.notifyrc:54
msgctxt "Name"
msgid "Indexing suspended"
msgstr "Indeksering midlertidig stoppet"

#. +> trunk stable
#: services/fileindexer/nepomukfileindexer.notifyrc:103
msgctxt "Comment"
msgid "File indexing has been suspended by the search service."
msgstr "Filindeksering er stoppet av søketjenesten."

#. +> trunk stable
#: services/fileindexer/nepomukfileindexer.notifyrc:155
msgctxt "Name"
msgid "Indexing resumed"
msgstr "Indeksering gjenopptatt"

#. +> trunk stable
#: services/fileindexer/nepomukfileindexer.notifyrc:204
msgctxt "Comment"
msgid "File indexing has been resumed by the search service."
msgstr "Filindeksering er gjenopptatt av søketjenesten."

#. +> trunk stable
#: services/filewatch/nepomukfilewatch.desktop:7
msgctxt "Name"
msgid "NepomukFileWatch"
msgstr "NepomukFileWatch"

#. +> trunk stable
#: services/filewatch/nepomukfilewatch.desktop:56
msgctxt "Comment"
msgid "The Nepomuk file watch service for monitoring file changes"
msgstr "Nepomuk-tjeneste som overvåker filer etter endringer"

#. +> trunk stable
#: services/filewatch/nepomukfilewatch.notifyrc:3
msgctxt "Comment"
msgid "Nepomuk file watch service"
msgstr "Nepomuk filovervåking"

#. +> trunk stable
#: services/filewatch/nepomukfilewatch.notifyrc:54
msgctxt "Name"
msgid "New Removable Device"
msgstr "Ny flyttbar enhet"

#. +> trunk stable
#: services/filewatch/nepomukfilewatch.notifyrc:103
msgctxt "Comment"
msgid "A new unknown removable device has been mounted"
msgstr "En ny ukjent flyttbar enhet er montert"

#. +> trunk stable
#: services/filewatch/org.kde.nepomuk.filewatch.actions:2
msgctxt "Name"
msgid "Folder Watch Limit"
msgstr "Grense for mappeovervåking"

#. +> trunk stable
#: services/filewatch/org.kde.nepomuk.filewatch.actions:42
msgctxt "Description"
msgid "To avoid missing file changes, raise the folder watch limit"
msgstr "For å få med alle filendringer kan grensa for mappeovervåking økes"

#. +> trunk stable
#: services/storage/backup/gui/nepomukbackup.desktop:2
msgctxt "Name"
msgid "Nepomuk Backup"
msgstr "Nepomuk sikringskopiering"

#. +> trunk stable
#: services/storage/nepomukstorage.desktop:7
msgctxt "Name"
msgid "Nepomuk Data Storage"
msgstr "Nepomuk datalager"

#. +> trunk stable
#: services/storage/nepomukstorage.desktop:56
msgctxt "Comment"
msgid "The Core Nepomuk data storage service"
msgstr "Sentral datalagertjeneste for Nepomuk"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:3
msgctxt "Name"
msgid "Semantic Data Storage"
msgstr "Semantisk datalager"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:52
msgctxt "Comment"
msgid "Semantic Desktop"
msgstr "Semantisk skrivebord"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:103
msgctxt "Name"
msgid "Failed to start Nepomuk"
msgstr "Klarte ikke å starte Nepomuk"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:152
msgctxt "Comment"
msgid "The Nepomuk Semantic Desktop system could not be started"
msgstr "Nepomuk semantisk skrivebordsystem kunne ikke startes"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:204
msgctxt "Name"
msgid "Converting Nepomuk data"
msgstr "Omformer Nepomuk-data"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:253
msgctxt "Comment"
msgid "All Nepomuk data is converted to a new storage backend"
msgstr "Alle Nepomuk-data er omformet til en ny lagringsmotor"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:305
msgctxt "Name"
msgid "Converting Nepomuk data failed"
msgstr "Nepomuk dataomforming mislyktes"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:354
msgctxt "Comment"
msgid "Converting Nepomuk data to a new backend failed"
msgstr "Det lyktes ikke å omforme Nepomuk-data til en ny bakgrunnsmotor"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:406
msgctxt "Name"
msgid "Converting Nepomuk data done"
msgstr "Nepomuk dataomforming fullført"

#. +> trunk stable
#: services/storage/nepomukstorage.notifyrc:455
msgctxt "Comment"
msgid "Successfully converted Nepomuk data to new backend"
msgstr "Nepomuk-data er vellykket omformet til en ny lagringsmotor"

#~ msgctxt "Name"
#~ msgid "Initial Indexing started"
#~ msgstr "Første indeksering startet"

#~ msgctxt "Name"
#~ msgid "Initial Indexing finished"
#~ msgstr "Første indeksering fullført"

#~ msgctxt "Comment"
#~ msgid "Indexing of local files for fast searches has started."
#~ msgstr "Indeksering av lokale filer for raske søk er startet."

#~ msgctxt "Comment"
#~ msgid "The initial indexing of local files for fast desktop searches has completed."
#~ msgstr "Første indeksering av lokale filer for raske søk er fullført."

#~ msgctxt "Name"
#~ msgid "Nepomuk Backup and Sync"
#~ msgstr "Nepomuk sikringskopiering og synkronisering"

#~ msgctxt "Comment"
#~ msgid "Nepomuk Service which handles backup and sync."
#~ msgstr "Nepomuk-tjeneste som håndterer sikringskopi og synkronisering."

#~ msgctxt "Name"
#~ msgid "NepomukQueryService"
#~ msgstr "NepomukQueryService"

#~ msgctxt "Comment"
#~ msgid "The Nepomuk Query Service provides an interface for persistent query folders"
#~ msgstr "Nepomuk spørretjeneste gir et grensesnitt til varige spørremapper"

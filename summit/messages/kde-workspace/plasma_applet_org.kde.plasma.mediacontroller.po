# Translation of plasma_applet_org.kde.plasma.mediacontroller to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-08 09:24+0200\n"
"PO-Revision-Date: 2014-09-22 19:17+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
#: contents/ui/ExpandedRepresentation.qml:100 contents/ui/main.qml:61
msgid "No media playing"
msgstr "Ingen medier blir spilt"

#. +> trunk5 stable5
#: contents/ui/ExpandedRepresentation.qml:173 contents/ui/main.qml:115
#, kde-format
msgctxt "Bring the window of player %1 to the front"
msgid "Open %1"
msgstr "Åpne %1"

#. +> trunk5 stable5
#: contents/ui/main.qml:152
#, kde-format
msgctxt "Artist of the song"
msgid "by %1"
msgstr "av %1"

#. +> trunk5 stable5
#: contents/ui/main.qml:163
#, kde-format
msgctxt "Artist of the song"
msgid "by %1 (paused)"
msgstr "av %1 (Pause)"

#. +> trunk5 stable5
#: contents/ui/main.qml:163
msgid "Paused"
msgstr "Pause"

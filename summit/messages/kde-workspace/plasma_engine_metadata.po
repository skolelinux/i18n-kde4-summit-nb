# Translation of plasma_engine_metadata to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2013-12-17 08:56+0100\n"
"PO-Revision-Date: 2009-08-11 10:26+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> stable
#: metadata_engine.cpp:70 metadata_engine.cpp:122
msgctxt "@label"
msgid "Size"
msgstr "Størrelse"

#. +> stable
#: metadata_engine.cpp:73 metadata_engine.cpp:116
msgctxt "@label"
msgid "Source Modified"
msgstr "Kilde endret"

#. +> stable
#: metadata_engine.cpp:112
msgctxt "@label"
msgid "Model"
msgstr "Modell"

#. +> stable
#: metadata_engine.cpp:113
msgctxt "@label"
msgid "Focal Length"
msgstr "Brennvidde"

#. +> stable
#: metadata_engine.cpp:114
msgctxt "@label"
msgid "Mime Type"
msgstr "Mime-type"

#. +> stable
#: metadata_engine.cpp:115
msgctxt "@label"
msgid "Manufacturer"
msgstr "Produsent"

#. +> stable
#: metadata_engine.cpp:117
msgctxt "@label"
msgid "Orientation"
msgstr "Orientering"

#. +> stable
#: metadata_engine.cpp:118
msgctxt "@label"
msgid "Flash Used"
msgstr "Flash brukt"

#. +> stable
#: metadata_engine.cpp:119
msgctxt "@label"
msgid "Height"
msgstr "Høyde"

#. +> stable
#: metadata_engine.cpp:120
msgctxt "@label"
msgid "Width"
msgstr "Bredde"

#. +> stable
#: metadata_engine.cpp:121
msgctxt "@label"
msgid "Url"
msgstr "URL"

#. +> stable
#: metadata_engine.cpp:123
msgctxt "@label"
msgid "Aperture"
msgstr "Blender"

#. +> stable
#: metadata_engine.cpp:124
msgctxt "@label"
msgid "Metering Mode"
msgstr "Målemodus"

#. +> stable
#: metadata_engine.cpp:125
msgctxt "@label"
msgid "35mm Equivalent"
msgstr "35mm ekvivalent"

#. +> stable
#: metadata_engine.cpp:126
msgctxt "@label"
msgid "File Extension"
msgstr "Filetternavn"

#. +> stable
#: metadata_engine.cpp:127
msgctxt "@label"
msgid "Name"
msgstr "Navn"

#. +> stable
#: metadata_engine.cpp:128
msgctxt "@label"
msgid "Exposure Time"
msgstr "Eksponeringstid"

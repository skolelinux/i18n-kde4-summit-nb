# Translation of plasma_applet_org.kde.plasma.fuzzyclock to Norwegian Bokmål
#
# Karl Ove Hufthammer <karl@huftis.org>, 2008.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009, 2014.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_fuzzy_clock\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-08 09:24+0200\n"
"PO-Revision-Date: 2014-10-02 11:03+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5 stable
#: package/contents/config/config.qml:27
#: package/contents/ui/configAppearance.qml:39
msgid "Appearance"
msgstr "Utseende"

#. +> trunk5 stable5
#: package/contents/ui/configAppearance.qml:45
msgid "Bold text"
msgstr "Halvfet tekst"

#. +> trunk5 stable5
#: package/contents/ui/configAppearance.qml:50
msgid "Italic text"
msgstr "Kursiv tekst"

#. +> trunk5 stable5
#: package/contents/ui/configAppearance.qml:56
msgid "Fuzzyness"
msgstr "Uskarphet"

#. +> trunk5 stable5
#: package/contents/ui/configAppearance.qml:61
msgid "Accurate"
msgstr "Nøyaktig"

#. +> trunk5 stable5
#: package/contents/ui/configAppearance.qml:74
msgid "Fuzzy"
msgstr "Uklar"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:43
msgid "One o’clock"
msgstr "Klokka ett"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:44
msgid "Five past one"
msgstr "Fem over ett"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:45
msgid "Ten past one"
msgstr "Ti over ett"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:46
msgid "Quarter past one"
msgstr "Kvart over ett"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:47
msgid "Twenty past one"
msgstr "Ti på halv to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:48
#, fuzzy
#| msgid "Twenty five past one"
msgid "Twenty-five past one"
msgstr "Fem på halv to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:49
msgid "Half past one"
msgstr "Halv to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:50
#, fuzzy
#| msgid "Twenty five to two"
msgid "Twenty-five to two"
msgstr "Fem over halv to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:51
msgid "Twenty to two"
msgstr "Ti over halv to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:52
msgid "Quarter to two"
msgstr "Kvart på to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:53
msgid "Ten to two"
msgstr "Ti på to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:54
msgid "Five to two"
msgstr "Fem på to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:55
msgid "Two o’clock"
msgstr "Klokka to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:56
msgid "Five past two"
msgstr "Fem over to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:57
msgid "Ten past two"
msgstr "Ti over to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:58
msgid "Quarter past two"
msgstr "Kvart over to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:59
msgid "Twenty past two"
msgstr "Ti på halv tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:60
#, fuzzy
#| msgid "Twenty five past two"
msgid "Twenty-five past two"
msgstr "Fem på halv tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:61
msgid "Half past two"
msgstr "Halv tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:62
#, fuzzy
#| msgid "Twenty five to three"
msgid "Twenty-five to three"
msgstr "Fem over halv fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:63
msgid "Twenty to three"
msgstr "Ti over halv fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:64
msgid "Quarter to three"
msgstr "Kvart på tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:65
msgid "Ten to three"
msgstr "Ti på tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:66
msgid "Five to three"
msgstr "Fem på tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:67
msgid "Three o’clock"
msgstr "Klokka tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:68
msgid "Five past three"
msgstr "Fem over tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:69
msgid "Ten past three"
msgstr "Ti over tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:70
msgid "Quarter past three"
msgstr "Kvart over tre"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:71
msgid "Twenty past three"
msgstr "Ti på halv fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:72
#, fuzzy
#| msgid "Twenty five past three"
msgid "Twenty-five past three"
msgstr "Fem på halv fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:73
msgid "Half past three"
msgstr "Halv fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:74
#, fuzzy
#| msgid "Twenty five to four"
msgid "Twenty-five to four"
msgstr "Fem over halv fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:75
msgid "Twenty to four"
msgstr "Ti over halv  fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:76
msgid "Quarter to four"
msgstr "Kvart på fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:77
msgid "Ten to four"
msgstr "Ti på fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:78
msgid "Five to four"
msgstr "Fem på fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:79
msgid "Four o’clock"
msgstr "Klokka fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:80
msgid "Five past four"
msgstr "Fem over fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:81
msgid "Ten past four"
msgstr "Ti over fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:82
msgid "Quarter past four"
msgstr "Kvart over fire"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:83
msgid "Twenty past four"
msgstr "Ti på halv fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:84
#, fuzzy
#| msgid "Twenty five past four"
msgid "Twenty-five past four"
msgstr "Fem på halv fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:85
msgid "Half past four"
msgstr "Halv fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:86
#, fuzzy
#| msgid "Twenty five to five"
msgid "Twenty-five to five"
msgstr "Fem over halv fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:87
msgid "Twenty to five"
msgstr "Ti over halv fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:88
msgid "Quarter to five"
msgstr "Kvart på fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:89
msgid "Ten to five"
msgstr "Ti på fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:90
msgid "Five to five"
msgstr "Fem på fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:91
msgid "Five o’clock"
msgstr "Klokka fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:92
msgid "Five past five"
msgstr "Fem over fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:93
msgid "Ten past five"
msgstr "Ti over fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:94
msgid "Quarter past five"
msgstr "Kvart over fem"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:95
msgid "Twenty past five"
msgstr "Ti på halv seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:96
#, fuzzy
#| msgid "Twenty five past five"
msgid "Twenty-five past five"
msgstr "Fem på halv seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:97
msgid "Half past five"
msgstr "Halv seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:98
#, fuzzy
#| msgid "Twenty five to six"
msgid "Twenty-five to six"
msgstr "Fem over halv seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:99
msgid "Twenty to six"
msgstr "Ti over halv seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:100
msgid "Quarter to six"
msgstr "Kvart på seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:101
msgid "Ten to six"
msgstr "Ti på seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:102
msgid "Five to six"
msgstr "Fem på seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:103
msgid "Six o’clock"
msgstr "Klokka seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:104
msgid "Five past six"
msgstr "Fem over seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:105
msgid "Ten past six"
msgstr "Ti over seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:106
msgid "Quarter past six"
msgstr "Kvart over seks"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:107
msgid "Twenty past six"
msgstr "Ti på halv sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:108
#, fuzzy
#| msgid "Twenty five past six"
msgid "Twenty-five past six"
msgstr "Fem på halv sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:109
msgid "Half past six"
msgstr "Halv sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:110
#, fuzzy
#| msgid "Twenty five to seven"
msgid "Twenty-five to seven"
msgstr "Fem over halv åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:111
msgid "Twenty to seven"
msgstr "Ti over halv åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:112
msgid "Quarter to seven"
msgstr "Kvart på sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:113
msgid "Ten to seven"
msgstr "Ti på sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:114
msgid "Five to seven"
msgstr "Fem på sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:115
msgid "Seven o’clock"
msgstr "Klokka sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:116
msgid "Five past seven"
msgstr "Fem over sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:117
msgid "Ten past seven"
msgstr "Ti over sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:118
msgid "Quarter past seven"
msgstr "Kvart over sju"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:119
msgid "Twenty past seven"
msgstr "Ti på halv åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:120
#, fuzzy
#| msgid "Twenty five past seven"
msgid "Twenty-five past seven"
msgstr "Fem på halv åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:121
msgid "Half past seven"
msgstr "Halv åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:122
#, fuzzy
#| msgid "Twenty five to eight"
msgid "Twenty-five to eight"
msgstr "Fem over halv åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:123
msgid "Twenty to eight"
msgstr "Ti over halv åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:124
msgid "Quarter to eight"
msgstr "Kvart på åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:125
msgid "Ten to eight"
msgstr "Ti på åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:126
msgid "Five to eight"
msgstr "Fem på åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:127
msgid "Eight o’clock"
msgstr "Klokka åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:128
msgid "Five past eight"
msgstr "Fem over åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:129
msgid "Ten past eight"
msgstr "Ti over åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:130
msgid "Quarter past eight"
msgstr "Kvart over åtte"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:131
msgid "Twenty past eight"
msgstr "Ti på halv ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:132
#, fuzzy
#| msgid "Twenty five past eight"
msgid "Twenty-five past eight"
msgstr "Fem på halv ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:133
msgid "Half past eight"
msgstr "Halv ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:134
#, fuzzy
#| msgid "Twenty five to nine"
msgid "Twenty-five to nine"
msgstr "Fem over halv ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:135
msgid "Twenty to nine"
msgstr "Ti over halv ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:136
msgid "Quarter to nine"
msgstr "Kvart på ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:137
msgid "Ten to nine"
msgstr "Ti på ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:138
msgid "Five to nine"
msgstr "Fem på ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:139
msgid "Nine o’clock"
msgstr "Klokka ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:140
msgid "Five past nine"
msgstr "Fem over ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:141
msgid "Ten past nine"
msgstr "Ti over ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:142
msgid "Quarter past nine"
msgstr "Kvart over ni"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:143
msgid "Twenty past nine"
msgstr "Ti på halv ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:144
#, fuzzy
#| msgid "Twenty five past nine"
msgid "Twenty-five past nine"
msgstr "Fem på halv ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:145
msgid "Half past nine"
msgstr "Halv ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:146
#, fuzzy
#| msgid "Twenty five to ten"
msgid "Twenty-five to ten"
msgstr "Fem over halv ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:147
msgid "Twenty to ten"
msgstr "Ti over halv ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:148
msgid "Quarter to ten"
msgstr "Kvart på ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:149
msgid "Ten to ten"
msgstr "Ti på ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:150
msgid "Five to ten"
msgstr "Fem på ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:151
msgid "Ten o’clock"
msgstr "Klokka ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:152
msgid "Five past ten"
msgstr "Fem over ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:153
msgid "Ten past ten"
msgstr "Ti over ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:154
msgid "Quarter past ten"
msgstr "Kvart over ti"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:155
msgid "Twenty past ten"
msgstr "Ti på halv elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:156
#, fuzzy
#| msgid "Twenty five past ten"
msgid "Twenty-five past ten"
msgstr "Fem på halv elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:157
msgid "Half past ten"
msgstr "Halv elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:158
#, fuzzy
#| msgid "Twenty five to eleven"
msgid "Twenty-five to eleven"
msgstr "Fem over halv elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:159
msgid "Twenty to eleven"
msgstr "Ti over halv elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:160
msgid "Quarter to eleven"
msgstr "Kvart på elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:161
msgid "Ten to eleven"
msgstr "Ti på elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:162
msgid "Five to eleven"
msgstr "Fem på elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:163
msgid "Eleven o’clock"
msgstr "Klokka elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:164
msgid "Five past eleven"
msgstr "Fem over elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:165
msgid "Ten past eleven"
msgstr "Ti over elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:166
msgid "Quarter past eleven"
msgstr "Kvart over elleve"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:167
msgid "Twenty past eleven"
msgstr "Ti på halv tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:168
#, fuzzy
#| msgid "Twenty five past eleven"
msgid "Twenty-five past eleven"
msgstr "Fem på halv tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:169
msgid "Half past eleven"
msgstr "Halv tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:170
#, fuzzy
#| msgid "Twenty five to twelve"
msgid "Twenty-five to twelve"
msgstr "Fem over halv tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:171
msgid "Twenty to twelve"
msgstr "Ti over halv tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:172
msgid "Quarter to twelve"
msgstr "Kvart på tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:173
msgid "Ten to twelve"
msgstr "Ti på tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:174
msgid "Five to twelve"
msgstr "Fem på tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:175
msgid "Twelve o’clock"
msgstr "Klokka tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:176
msgid "Five past twelve"
msgstr "Fem over tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:177
msgid "Ten past twelve"
msgstr "Ti over tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:178
msgid "Quarter past twelve"
msgstr "Kvart over tolv"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:179
msgid "Twenty past twelve"
msgstr "Ti på halv ett"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:180
#, fuzzy
#| msgid "Twenty five past twelve"
msgid "Twenty-five past twelve"
msgstr "Fem på halv ett"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:181
msgid "Half past twelve"
msgstr "Halv ett"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:182
#, fuzzy
#| msgid "Twenty five to one"
msgid "Twenty-five to one"
msgstr "Fem over halv to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:183
msgid "Twenty to one"
msgstr "Ti over halv to"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:184
msgid "Quarter to one"
msgstr "Kvart på ett"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:185
msgid "Ten to one"
msgstr "Ti på ett"

#. +> trunk5 stable5
#: package/contents/ui/FuzzyClock.qml:186
msgid "Five to one"
msgstr "Fem på ett"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:190
msgid "Night"
msgstr "Natt"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:190
msgid "Early morning"
msgstr "Tidlig om morgenen"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:190
msgid "Morning"
msgstr "Morgen"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:190
msgid "Almost noon"
msgstr "Snart tolv"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:191
msgid "Noon"
msgstr "Tolv"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:191
msgid "Afternoon"
msgstr "Ettermiddag"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:191
msgid "Evening"
msgstr "Kveld"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:191
msgid "Late evening"
msgstr "Sent om kvelden"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:195
msgid "Start of week"
msgstr "Tidlig i uka"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:195
msgid "Middle of week"
msgstr "Midt i uka"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:195
msgid "End of week"
msgstr "Slutten av uka"

#. +> trunk5 stable5 stable
#: package/contents/ui/FuzzyClock.qml:195
msgid "Weekend!"
msgstr "Helg!"

#. +> stable
#: fuzzyClock.cpp:207
msgid "General"
msgstr "Generelt"

#. +> stable
#: fuzzyClock.cpp:310
#, kde-format
msgctxt "@label Short date: %1 day in the month, %2 short month name, %3 year"
msgid "%1 %2 %3"
msgstr "%1. %2 %3"

#. +> stable
#: fuzzyClock.cpp:315
#, kde-format
msgctxt "@label Short date: %1 day in the month, %2 short month name"
msgid "%1 %2"
msgstr "%1. %2"

#. +> stable
#: fuzzyClock.cpp:321
#, kde-format
msgctxt "@label Day of the week with date: %1 short day name, %2 short date"
msgid "%1, %2"
msgstr "%1 %2"

#. +> stable
#: fuzzyClock.cpp:340
msgctxt "hour in the messages below"
msgid "one"
msgstr "ett"

#. +> stable
#: fuzzyClock.cpp:341
msgctxt "hour in the messages below"
msgid "two"
msgstr "to"

#. +> stable
#: fuzzyClock.cpp:342
msgctxt "hour in the messages below"
msgid "three"
msgstr "tre"

#. +> stable
#: fuzzyClock.cpp:343
msgctxt "hour in the messages below"
msgid "four"
msgstr "fire"

#. +> stable
#: fuzzyClock.cpp:344
msgctxt "hour in the messages below"
msgid "five"
msgstr "fem"

#. +> stable
#: fuzzyClock.cpp:345
msgctxt "hour in the messages below"
msgid "six"
msgstr "seks"

#. +> stable
#: fuzzyClock.cpp:346
msgctxt "hour in the messages below"
msgid "seven"
msgstr "sju"

#. +> stable
#: fuzzyClock.cpp:347
msgctxt "hour in the messages below"
msgid "eight"
msgstr "åtte"

#. +> stable
#: fuzzyClock.cpp:348
msgctxt "hour in the messages below"
msgid "nine"
msgstr "ni"

#. +> stable
#: fuzzyClock.cpp:349
msgctxt "hour in the messages below"
msgid "ten"
msgstr "ti"

#. +> stable
#: fuzzyClock.cpp:350
msgctxt "hour in the messages below"
msgid "eleven"
msgstr ""
"elleve "
"|/|"
" $[egenskap etter tolv]"

#. +> stable
#: fuzzyClock.cpp:351
msgctxt "hour in the messages below"
msgid "twelve"
msgstr ""
"tolv "
"|/|"
" $[egenskap etter ett]"

#. +> stable
#: fuzzyClock.cpp:353 fuzzyClock.cpp:365
#, kde-format
msgctxt "%1 the hour translated above"
msgid "%1 o'clock"
msgstr "klokka %1"

#. +> stable
#: fuzzyClock.cpp:354
#, kde-format
msgctxt "%1 the hour translated above"
msgid "five past %1"
msgstr "fem over %1"

#. +> stable
#: fuzzyClock.cpp:355
#, kde-format
msgctxt "%1 the hour translated above"
msgid "ten past %1"
msgstr "ti over %1"

#. +> stable
#: fuzzyClock.cpp:356
#, kde-format
msgctxt "%1 the hour translated above"
msgid "quarter past %1"
msgstr "kvart over %1"

#. +> stable
#: fuzzyClock.cpp:357
#, kde-format
msgctxt "%1 the hour translated above"
msgid "twenty past %1"
msgstr ""
"%1 tjue "
"|/|"
" ti på halv $[etter %1]"

#. +> stable
#: fuzzyClock.cpp:358
#, kde-format
msgctxt "%1 the hour translated above"
msgid "twenty five past %1"
msgstr ""
"%1 tjuefem "
"|/|"
" fem på halv $[etter %1]"

#. +> stable
#: fuzzyClock.cpp:359
#, kde-format
msgctxt "%1 the hour translated above"
msgid "half past %1"
msgstr ""
"%1 tretti "
"|/|"
" halv $[etter %1]"

#. +> stable
#: fuzzyClock.cpp:360
#, kde-format
msgctxt "%1 the hour translated above"
msgid "twenty five to %1"
msgstr "fem over halv %1"

#. +> stable
#: fuzzyClock.cpp:361
#, kde-format
msgctxt "%1 the hour translated above"
msgid "twenty to %1"
msgstr "ti over halv %1"

#. +> stable
#: fuzzyClock.cpp:362
#, kde-format
msgctxt "%1 the hour translated above"
msgid "quarter to %1"
msgstr "kvart på %1"

#. +> stable
#: fuzzyClock.cpp:363
#, kde-format
msgctxt "%1 the hour translated above"
msgid "ten to %1"
msgstr "ti på %1"

#. +> stable
#: fuzzyClock.cpp:364
#, kde-format
msgctxt "%1 the hour translated above"
msgid "five to %1"
msgstr "fem på %1"

#. +> stable
#: fuzzyClock.cpp:392
msgctxt "Whether to uppercase the first letter of completed fuzzy time strings above: translate as 1 if yes, 0 if no."
msgid "1"
msgstr "1"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#. +> stable
#: fuzzyClockConfig.ui:46
msgid "Font style:"
msgstr "Skriftstil:"

#. i18n: ectx: property (toolTip), widget (QCheckBox, fontTimeBold)
#. +> stable
#: fuzzyClockConfig.ui:79
msgid "Check if you want the font in bold"
msgstr "Kryss av her hvis du vil ha halvfet skrift"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, fontTimeBold)
#. +> stable
#: fuzzyClockConfig.ui:82
msgid "When this is checked, the clock font will be bold."
msgstr "Hvis dette er krysset av blir klokkeskrifta halvfet."

#. i18n: ectx: property (text), widget (QCheckBox, fontTimeBold)
#. +> stable
#: fuzzyClockConfig.ui:85
msgid "&Bold"
msgstr "&Halvfet"

#. i18n: ectx: property (toolTip), widget (QCheckBox, fontTimeItalic)
#. +> stable
#: fuzzyClockConfig.ui:92
msgid "Check if you want the font in italic"
msgstr "Kryss av her om du vil ha kursiv skrift"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, fontTimeItalic)
#. +> stable
#: fuzzyClockConfig.ui:95
msgid "When this is checked, the clock font will be in italic."
msgstr "Hvis dette er krysset av blir klokkeskrifta kursiv."

#. i18n: ectx: property (text), widget (QCheckBox, fontTimeItalic)
#. +> stable
#: fuzzyClockConfig.ui:98
msgid "&Italic"
msgstr "Kurs&iv"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#. +> stable
#: fuzzyClockConfig.ui:105
msgid "Font color:"
msgstr "Skriftfarge:"

#. i18n: ectx: property (toolTip), widget (QRadioButton, useThemeColor)
#. +> stable
#: fuzzyClockConfig.ui:124
msgid "Use current desktop theme color"
msgstr "Bruk skrivebordets gjeldende temafarge"

#. i18n: ectx: property (whatsThis), widget (QRadioButton, useThemeColor)
#. +> stable
#: fuzzyClockConfig.ui:127
msgid "This is default. The clock will get its font color from the current desktop theme."
msgstr "Dette er standard. Klokka får sin skriftfarge fra det gjeldende skrivebordstemaet."

#. i18n: ectx: property (text), widget (QRadioButton, useThemeColor)
#. +> stable
#: fuzzyClockConfig.ui:130
msgid "Use theme color"
msgstr "Bruk temafarge"

#. i18n: ectx: property (toolTip), widget (QRadioButton, useCustomFontColor)
#. +> stable
#: fuzzyClockConfig.ui:148
msgid "Choose your own font color"
msgstr "Velg din egen skriftfarge"

#. i18n: ectx: property (whatsThis), widget (QRadioButton, useCustomFontColor)
#. +> stable
#: fuzzyClockConfig.ui:151
msgid "When checked you can choose a custom color for the clock font by clicking on the color widget on the right."
msgstr "Når dette er krysset av kan du velge en egen farge for klokkeskrifta ved å trykke på fargeelementet til høyre."

#. i18n: ectx: property (text), widget (QRadioButton, useCustomFontColor)
#. +> stable
#: fuzzyClockConfig.ui:154
msgid "Use custom color:"
msgstr "Bruk selvvalgt farge:"

#. i18n: ectx: property (toolTip), widget (KColorButton, fontColor)
#. +> stable
#: fuzzyClockConfig.ui:164
msgid "Color chooser"
msgstr "Fargevelger"

#. i18n: ectx: property (whatsThis), widget (KColorButton, fontColor)
#. +> stable
#: fuzzyClockConfig.ui:167
msgid "Click on this button and the KDE standard color dialog will show. You can then choose the new color you want for your clock."
msgstr "KDEs standard fargevelger vises om denne knappen trykkes. Der kan du så velge den nye fargen du vil ha på klokkeskrifta."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#. +> stable
#: fuzzyClockConfig.ui:189
msgid "Adjust text to panel-height:"
msgstr "Juster tekst til panelhøyde:"

#. i18n: ectx: property (toolTip), widget (QSlider, adjustToHeight)
#. +> stable
#: fuzzyClockConfig.ui:202
msgid "0: disable; 2: use full panel-height"
msgstr "0: ikke juster; 2: bruk hele panelhøyden"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#. +> stable
#: fuzzyClockConfig.ui:224
msgid "Information"
msgstr "Informasjon"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#. +> stable
#: fuzzyClockConfig.ui:231
msgid "Show date:"
msgstr "Vis dato:"

#. i18n: ectx: property (toolTip), widget (QCheckBox, showDate)
#. +> stable
#: fuzzyClockConfig.ui:246
msgid "Display the date of the day"
msgstr "Vis dagens dato"

#. i18n: ectx: property (toolTip), widget (QCheckBox, showDay)
#. +> stable
#: fuzzyClockConfig.ui:295
msgid "Display day of the week"
msgstr "Vis ukedagen"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, showDay)
#. +> stable
#: fuzzyClockConfig.ui:298
msgid "Add the day of the week to the date display."
msgstr "Legg til ukedag til datovisningen."

#. i18n: ectx: property (text), widget (QCheckBox, showDay)
#. +> stable
#: fuzzyClockConfig.ui:301
msgid "Show day of the &week"
msgstr "Vis &ukedag"

#. i18n: ectx: property (toolTip), widget (QCheckBox, showYear)
#. +> stable
#: fuzzyClockConfig.ui:327
msgid "Display the current year"
msgstr "Vis inneværende år"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, showYear)
#. +> stable
#: fuzzyClockConfig.ui:330
msgid "Add the year to the date string."
msgstr "Legg til årstall til datostrengen."

#. i18n: ectx: property (text), widget (QCheckBox, showYear)
#. +> stable
#: fuzzyClockConfig.ui:333
msgid "Show &year"
msgstr "Vis &år"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#. +> stable
#: fuzzyClockConfig.ui:342
msgid "Show time zone:"
msgstr "Vis tidssone:"

#. i18n: ectx: property (toolTip), widget (QCheckBox, showTimezone)
#. +> stable
#: fuzzyClockConfig.ui:357
msgid "Display the time zone name"
msgstr "Vis navnet på tidssonen"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, showTimezone)
#. +> stable
#: fuzzyClockConfig.ui:360
msgid "Display the time zone name under the time."
msgstr "Vis navnet på tidssonen under klokkeslettet."

#. i18n: ectx: property (text), widget (QLabel, label)
#. +> stable
#: fuzzyClockConfig.ui:385
msgid "Degree of fuzzyness:"
msgstr "Grad av uklarhet:"

#. i18n: ectx: property (toolTip), widget (QSlider, fuzzynessSlider)
#. +> stable
#: fuzzyClockConfig.ui:398
msgid "1: least fuzzy"
msgstr "1: minst uklar"

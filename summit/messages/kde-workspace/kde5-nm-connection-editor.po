# Translation of kde5-nm-connection-editor to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-08 09:23+0200\n"
"PO-Revision-Date: 2015-08-27 17:03+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Steensrud"

#. +> trunk5 stable5
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjornst@skogkatt.homelinux.org"

#. +> trunk5 stable5
#: connectioneditor.cpp:83
msgid "Type here to search connections..."
msgstr "Skriv her for å søke i tilkoblinger …"

#. +> trunk5 stable5
#: connectioneditor.cpp:124
msgid "Add"
msgstr "Legg til"

#. +> trunk5 stable5
#: connectioneditor.cpp:128
msgid "Hardware"
msgstr "Maskinvare"

#. +> trunk5 stable5
#: connectioneditor.cpp:131
msgid "DSL"
msgstr "DSL"

#. +> trunk5 stable5
#: connectioneditor.cpp:134
msgid "InfiniBand"
msgstr "InfiniBand"

#. +> trunk5 stable5
#: connectioneditor.cpp:138
msgid "Mobile Broadband..."
msgstr "Mobilt bredbånd …"

#. +> trunk5 stable5
#: connectioneditor.cpp:142
msgid "Wired Ethernet"
msgstr "Kablet Ethernet"

#. +> trunk5 stable5
#: connectioneditor.cpp:146
msgid "Wired Ethernet (shared)"
msgstr "Kablet Ethenet (delt)"

#. +> trunk5 stable5
#: connectioneditor.cpp:150
msgid "Wi-Fi"
msgstr "Wi-Fi"

#. +> trunk5 stable5
#: connectioneditor.cpp:154
msgid "Wi-Fi (shared)"
msgstr "Wi-Fi (delt)"

#. +> trunk5 stable5
#: connectioneditor.cpp:158
msgid "WiMAX"
msgstr "WiMAX"

#. +> trunk5 stable5
#: connectioneditor.cpp:162
msgctxt "Virtual hardware devices, eg Bridge, Bond"
msgid "Virtual"
msgstr "Virtuell"

#. +> trunk5 stable5
#: connectioneditor.cpp:164
msgid "Bond"
msgstr "Bond"

#. +> trunk5 stable5
#: connectioneditor.cpp:167
msgid "Bridge"
msgstr "Bru"

#. +> trunk5 stable5
#: connectioneditor.cpp:170
msgid "VLAN"
msgstr "VLAN"

#. +> trunk5 stable5
#: connectioneditor.cpp:174
msgid "Team"
msgstr "Lad"

#. +> trunk5 stable5
#: connectioneditor.cpp:179
msgid "VPN"
msgstr "VPN"

#. +> trunk5 stable5
#: connectioneditor.cpp:199
msgid "Connect"
msgstr "Koble til"

#. +> trunk5 stable5
#: connectioneditor.cpp:204
msgid "Disconnect"
msgstr "Koble fra"

#. +> trunk5 stable5
#: connectioneditor.cpp:209
msgid "Edit..."
msgstr "Rediger …"

#. +> trunk5 stable5
#: connectioneditor.cpp:214
msgid "Delete"
msgstr "Slett"

#. +> trunk5 stable5
#: connectioneditor.cpp:220
msgid "Import VPN..."
msgstr "Importer VPN …"

#. +> trunk5 stable5
#: connectioneditor.cpp:224
msgid "Export VPN..."
msgstr "Eksporter VPN …"

#. +> trunk5 stable5
#: connectioneditor.cpp:307
msgid "my_shared_connection"
msgstr "min_delte_tilkobling"

#. +> trunk5 stable5
#: connectioneditor.cpp:364
#, kde-format
msgid "Connection %1 has been added"
msgstr "Tilkobling %1 er lagt til"

#. +> trunk5 stable5
#: connectioneditor.cpp:460
#, kde-format
msgid "Do you want to remove the connection '%1'?"
msgstr "Vil du slette tilkoblingen «%1» ?"

#. +> trunk5 stable5
#: connectioneditor.cpp:460
msgid "Remove Connection"
msgstr "Fjern tilkobling"

#. +> trunk5 stable5
#: connectioneditor.cpp:464
#, kde-format
#| msgid "Do you want to remove the connection '%1'?"
msgid "Do you want to remove the following connections: %1"
msgstr "Vil du fjerne følgende tilkoblinger: %1"

#. +> trunk5 stable5
#: connectioneditor.cpp:464
#| msgid "Remove Connection"
msgid "Remove Connections"
msgstr "Fjern tilkoblinger"

#. +> trunk5 stable5
#: connectioneditor.cpp:630
msgid "Import VPN Connection"
msgstr "Importer VPN-tilkopling"

#. +> trunk5 stable5
#: connectioneditor.cpp:657
#, kde-format
msgid ""
"Importing VPN connection %1 failed\n"
"%2"
msgstr ""
"Klarte ikke å importere VPN-tilkobling %1\n"
"%2"

#. +> trunk5 stable5
#: connectioneditor.cpp:701
msgid "Export is not supported by this VPN type"
msgstr "VPN-typen støtter ikke eksport"

#. +> trunk5 stable5
#: connectioneditor.cpp:707
msgid "Export VPN Connection"
msgstr "Eksporter VPN-tilkobling"

#. +> trunk5 stable5
#: connectioneditor.cpp:712
#, kde-format
msgid ""
"Exporting VPN connection %1 failed\n"
"%2"
msgstr ""
"Klarte ikke å eksportere VPN-tilkoblingen %1\n"
"%2"

#. +> trunk5 stable5
#: connectioneditor.cpp:717
#, kde-format
msgid "VPN connection %1 exported successfully"
msgstr "VPN-tilkopling %1 vellykket eksportert"

#. i18n: ectx: Menu (connection)
#. +> trunk5 stable5
#: kde5-nm-connection-editorui.rc:16
msgid "Connection"
msgstr "Tilkobling"

#. i18n: ectx: ToolBar (mainToolBar)
#. +> trunk5 stable5
#: kde5-nm-connection-editorui.rc:26
msgid "Main Toolbar"
msgstr "Hovedverktøylinje"

#. +> trunk5 stable5
#: main.cpp:40
msgid "Connection editor"
msgstr "Tilkoblingsredigering"

#. +> trunk5 stable5
#: main.cpp:41
msgid "Manage your network connections"
msgstr "Håndter nettverkstilkoblinger"

#. +> trunk5 stable5
#: main.cpp:42
msgid "(C) 2013-2015 Jan Grulich and Lukáš Tinkl"
msgstr "@ 2013-2015 Jan Grulich og Lukáš Tinkl"

#. +> trunk5 stable5
#: main.cpp:43
#, kde-format
msgid ""
"This application allows you to create, edit and delete network connections.\n"
"\n"
"Using NM version: %1"
msgstr ""
"Med dette programmet kan du opprette, redigere og slette negttverkstilkoblinger.\n"
"\n"
"Bruker NM-versjon: %1"

#. +> trunk5 stable5
#: main.cpp:44
msgid "Jan Grulich"
msgstr "Jan Grulich"

#. +> trunk5 stable5
#: main.cpp:44 main.cpp:45
msgid "Developer"
msgstr "Utvikler"

#. +> trunk5 stable5
#: main.cpp:45
msgid "Lukáš Tinkl"
msgstr "Lukáš Tinkl"

#. +> trunk5 stable5
#: main.cpp:46
msgid "Lamarque Souza"
msgstr "Lamarque Souza"

#. +> trunk5 stable5
#: main.cpp:46
msgid "libnm-qt author"
msgstr "libnm-qt utvikler"

#. +> trunk5 stable5
#: main.cpp:47
msgid "Daniel Nicoletti"
msgstr "Daniel Nicoletti"

#. +> trunk5 stable5
#: main.cpp:47
msgid "various bugfixes"
msgstr "diverse feilrettelser"

#. +> trunk5 stable5
#: main.cpp:48
msgid "Will Stephenson"
msgstr "Will Stephenson"

#. +> trunk5 stable5
#: main.cpp:48 main.cpp:49
msgid "VPN plugins"
msgstr "VPN-programtillegg"

#. +> trunk5 stable5
#: main.cpp:49
msgid "Ilia Kats"
msgstr "Ilia Kats"

#. i18n: ectx: property (windowTitle), widget (QWidget, ConnectionEditor)
#. +> trunk5 stable5
#: ui/connectioneditor.ui:20
msgid "Connection Editor"
msgstr "Tilkoblingsredigering"

#~ msgid "Wireless"
#~ msgstr "Trådløs"

#~ msgid "Wired"
#~ msgstr "Kablet"

#~ msgid "Wired (shared)"
#~ msgstr "kablet (delt)"

#~ msgid "Wireless (shared)"
#~ msgstr "Trådløs (delt)"

# Translation of libplasma_groupingcontainment to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-08 09:23+0200\n"
"PO-Revision-Date: 2010-11-24 09:43+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5 stable
#: abstractgroup.cpp:789
msgid "Group Configuration"
msgstr "Gruppeoppsett"

#. +> trunk5 stable5 stable
#: groupingcontainment.cpp:65
msgid "Add Groups"
msgstr "Legg til grupper"

#. +> trunk5 stable5 stable
#: groupingcontainment.cpp:940
#, kde-format
msgid "Remove this %1"
msgstr "Fjern denne %1"

#. +> trunk5 stable5 stable
#: groupingcontainment.cpp:944
#, kde-format
msgid "Configure this %1"
msgstr "Sett opp denne %1"

#. +> trunk5 stable5 stable
#: groups/floatinggroup.cpp:60
msgid "Floating Group"
msgstr "Flytende gruppe"

#. +> trunk5 stable5 stable
#: groups/flowgroup.cpp:392
msgid "Flow Group"
msgstr "Flytgruppe"

#. +> trunk5 stable5 stable
#: groups/gridgroup.cpp:652
msgid "Grid Group"
msgstr "Rutenett-gruppe"

#. +> trunk5 stable5 stable
#: groups/gridmanager.cpp:161 groups/gridmanager.cpp:163
msgid "Add a new column"
msgstr "Legg til en ny kolonne"

#. +> trunk5 stable5 stable
#: groups/gridmanager.cpp:162 groups/gridmanager.cpp:164
msgid "Remove a column"
msgstr "Fjern en kolonne"

#. +> trunk5 stable5 stable
#: groups/gridmanager.cpp:167 groups/gridmanager.cpp:169
msgid "Add a new row"
msgstr "Legg til en ny rad"

#. +> trunk5 stable5 stable
#: groups/gridmanager.cpp:168 groups/gridmanager.cpp:170
msgid "Remove a row"
msgstr "Fjern en rad"

#. +> trunk5 stable5 stable
#: groups/stackinggroup.cpp:215
msgid "Stacking Group"
msgstr "Stablegruppe"

#. +> trunk5 stable5 stable
#: groups/tabbinggroup.cpp:173
msgctxt "a general page in the config dialog"
msgid "General"
msgstr "Generelt"

#. +> trunk5 stable5 stable
#: groups/tabbinggroup.cpp:198
msgid "New Tab"
msgstr "Ny fane"

#. +> trunk5 stable5 stable
#: groups/tabbinggroup.cpp:378
msgid "Tabbing Group"
msgstr "Fanegruppe"

#. i18n: ectx: property (windowTitle), widget (QWidget, TabbingGroupConfig)
#. i18n: ectx: property (text), widget (QLabel, label)
#. +> trunk5 stable5 stable
#: groups/tabbinggroup_config.ui:14 groups/tabbinggroup_config.ui:22
msgid "Pages"
msgstr "Sider"

#. i18n: ectx: property (text), widget (QPushButton, modButton)
#. +> trunk5 stable5 stable
#: groups/tabbinggroup_config.ui:45
msgid "Rename page"
msgstr "Endre sidenavn"

#. i18n: ectx: property (text), widget (QPushButton, upButton)
#. +> trunk5 stable5 stable
#: groups/tabbinggroup_config.ui:52
msgid "Move up"
msgstr "Flytt opp"

#. i18n: ectx: property (text), widget (QPushButton, downButton)
#. +> trunk5 stable5 stable
#: groups/tabbinggroup_config.ui:59
msgid "Move down"
msgstr "Flytt ned"

# Translation of plasma_applet_org.kde.plasma.pager to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-12 09:14+0200\n"
"PO-Revision-Date: 2014-08-28 20:04+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5 stable
#: package/contents/config/config.qml:25
msgid "General"
msgstr "Generelt"

#. i18n: ectx: property (windowTitle), widget (QWidget, pagerConfig)
#. +> stable
#: pagerConfig.ui:14
msgid "Configure Pager"
msgstr "Sett opp sidevelger"

#. +> trunk5 stable5
#: package/contents/ui/configGeneral.qml:84
#, fuzzy
msgid "Display:"
msgstr "Vis:"

#. i18n: ectx: property (text), widget (QLabel, displayLabel)
#. +> stable
#: pagerConfig.ui:41
msgid "Display text:"
msgstr "Vist tekst:"

#. i18n: ectx: property (text), widget (QLabel, label)
#. +> stable
#: pagerConfig.ui:81
msgid "Display icons:"
msgstr "Vis ikoner:"

#. +> trunk5 stable5 stable
#: package/contents/ui/configGeneral.qml:90
msgid "Desktop number"
msgstr "Skrivebord nummer"

#. +> trunk5 stable5 stable
#: package/contents/ui/configGeneral.qml:101
msgid "Desktop name"
msgstr "Skrivebordets navn"

#. +> trunk5 stable5 stable
#: package/contents/ui/configGeneral.qml:107
msgid "No text"
msgstr "Ingen tekst"

#. +> trunk5 stable5
#: package/contents/ui/configGeneral.qml:118
msgid "Icons"
msgstr "Ikoner"

#. +> trunk5 stable5 stable
#: package/contents/ui/configGeneral.qml:122
msgid "Selecting current desktop:"
msgstr "Velger gjeldende skrivebord"

#. +> trunk5 stable5 stable
#: package/contents/ui/configGeneral.qml:128
msgid "Does nothing"
msgstr "Gjør ingenting"

#. +> trunk5 stable5 stable
#: package/contents/ui/configGeneral.qml:139
msgid "Shows desktop"
msgstr "Viser skrivebordet"

#. +> trunk5 stable5 stable
#: package/contents/ui/configGeneral.qml:145
msgid "Shows the dashboard"
msgstr "Viser kontrollpulten"

#. +> trunk5 stable5
#: package/contents/ui/main.qml:98
msgid "Add Virtual Desktop"
msgstr "Legg til virtuelt skrivebort"

#. +> trunk5 stable5
#: package/contents/ui/main.qml:99
msgid "Remove Virtual Desktop"
msgstr "Fjern virtuelt skrivebord"

#. +> trunk5 stable5
#: package/contents/ui/main.qml:102
msgid "Configure Desktops"
msgstr "Sett opp skrivebord"

#. +> trunk5 stable5
#: package/contents/ui/main.qml:216
#, kde-format
msgid "%1 window"
msgid_plural "%1 windows"
msgstr[0] "%1 vindu"
msgstr[1] "%1 vinduer"

#. +> trunk5 stable5
#: package/contents/ui/main.qml:229
#, kde-format
msgid "and %1 other window"
msgid_plural "and %1 other windows"
msgstr[0] "og %1 annet vindu"
msgstr[1] "og %1 andre vinduer"

#. +> stable
#: pager.cpp:307
msgid "&Add Virtual Desktop"
msgstr "&Legg til virtuelt skrivebord"

#. +> stable
#: pager.cpp:310
msgid "&Remove Last Virtual Desktop"
msgstr "&Fjern siste virtuelle skrivebord"

#. +> stable
#: pager.cpp:877
#, kde-format
msgid "One window:"
msgid_plural "%1 windows:"
msgstr[0] "Ett vindu:"
msgstr[1] "%1 vinduer:"

#. +> stable
#: pager.cpp:881
#, kde-format
msgid "and 1 other"
msgid_plural "and %1 others"
msgstr[0] "og 1 annet"
msgstr[1] "og %1 andre"

#~ msgid "Display Text:"
#~ msgstr "Vis tekst:"

# Translation of gid-migrator to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-07-30 09:24+0200\n"
"PO-Revision-Date: 2014-09-23 21:46+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Steensrud"

#. +> trunk5 stable5
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjornst@skogkatt.homelinux.org"

#. +> trunk5 stable5
#: ../kmigratorbase.cpp:127
#, kde-format
msgid "Creating instance of type %1"
msgstr "Oppretter instans av type %1"

#. +> trunk5 stable5
#: ../migratorbase.cpp:141
msgid "Missing Identifier"
msgstr "Mangler identifikator"

#. +> trunk5 stable5
#: ../migratorbase.cpp:154
msgid "Failed to start migration because migrator is not ready"
msgstr "Klarte ikke å starte migrering fordi migrator ikke er klar"

#. +> trunk5 stable5
#: ../migratorbase.cpp:160
msgid "Starting migration..."
msgstr "Starter migrering …"

#. +> trunk5 stable5
#: ../migratorbase.cpp:201
msgid "Migration complete"
msgstr "Migrering fullført"

#. +> trunk5 stable5
#: ../migratorbase.cpp:205
msgid "Migration aborted"
msgstr "Migrering avbrutt"

#. +> trunk5 stable5
#: ../migratorbase.cpp:211
msgid "Migration failed"
msgstr "Migrering mislyktes"

#. +> trunk5 stable5
#: ../migratorbase.cpp:215
msgid "Migration paused"
msgstr "Migrering pauset"

#. +> trunk5 stable5
#: ../migratorbase.cpp:245
msgid "This migration has already been started once but was aborted"
msgstr "Denne migreringen er allerede startet en gang, men ble avbrutt"

#. +> trunk5 stable5
#: ../migratorbase.cpp:281
msgctxt "@info:status"
msgid "Not started"
msgstr "Ikke startet"

#. +> trunk5 stable5
#: ../migratorbase.cpp:282
msgctxt "@info:status"
msgid "Running..."
msgstr "Kjører …"

#. +> trunk5 stable5
#: ../migratorbase.cpp:283
msgctxt "@info:status"
msgid "Complete"
msgstr "Fullført"

#. +> trunk5 stable5
#: ../migratorbase.cpp:284
msgctxt "@info:status"
msgid "Aborted"
msgstr "Avbrutt"

#. +> trunk5 stable5
#: ../migratorbase.cpp:285
msgctxt "@info:status"
msgid "Paused"
msgstr "Pauset"

#. +> trunk5 stable5
#: ../migratorbase.cpp:286
msgctxt "@info:status"
msgid "Needs Update"
msgstr "Trenger oppdatering"

#. +> trunk5 stable5
#: ../migratorbase.cpp:287
msgctxt "@info:status"
msgid "Failed"
msgstr "Mislyktes"

#. +> trunk5 stable5
#: gidmigrator.cpp:38
#, kde-format
msgctxt "Name of the GID Migrator (intended for advanced users)."
msgid "GID Migrator: %1"
msgstr "GID Migrator: %1"

#. +> trunk5 stable5
#: gidmigrator.cpp:43
#, kde-format
msgid "Ensures that all items with the mimetype %1 have a GID if a GID extractor is available."
msgstr "Sikrer at alle elementer med mimetype %1 har en GID hvis en GID-ekstraktor er tilgjengelig."

#. +> trunk5 stable5
#: gidmigrator.cpp:65
#, kde-format
msgid "Migration failed: %1"
msgstr "Migrering mislyktes: %1"

#. +> trunk5 stable5
#: main.cpp:40
msgid "GID Migration Tool"
msgstr "GID migreringsverktøy"

#. +> trunk5 stable5
#: main.cpp:42
msgid "Migration of Akonadi Items to support GID"
msgstr "Migrering av Akonadi-elementer for å støtte GID"

#. +> trunk5 stable5
#: main.cpp:44
#, fuzzy
#| msgid "(c) 2013 the Akonadi developers"
msgid "(c) 2013-2015 the Akonadi developers"
msgstr "© 2013 Akonadi-utviklerne"

#. +> trunk5 stable5
#: main.cpp:46
msgid "Christian Mollekopf"
msgstr "Christian Mollekopf"

#. +> trunk5 stable5
#: main.cpp:46
msgid "Author"
msgstr "Forfatter"

#. +> trunk5 stable5
#: main.cpp:52
msgid "Show reporting dialog"
msgstr "Vis rapporteringsdialog"

#. +> trunk5 stable5
#: main.cpp:53
msgid "Show report only if changes were made"
msgstr "Vis rapport bare hvis det ble gjort endringer"

#. +> trunk5 stable5
#: main.cpp:54
msgid "MIME type to migrate"
msgstr "Mime-type som skal migreres"

#~ msgid "Mimetype to migrate"
#~ msgstr "Mimetype som skal migreres"

# Translation of akonadi_singlefile_resource to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009, 2010, 2011, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-07-31 09:24+0200\n"
"PO-Revision-Date: 2014-09-23 21:49+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
#: getcredentialsjob.cpp:54
msgid "Could not find account"
msgstr "Kunne ikke finne konto"

#. +> trunk5 stable5
#: getcredentialsjob.cpp:66
msgid "Could not find credentials"
msgstr "Kunne ikke finne akkreditiver"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#. +> trunk5 stable5
#: settingsdialog.ui:20
msgid "Directory"
msgstr "Mappe"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#. +> trunk5 stable5
#: settingsdialog.ui:26
msgid "Directory Name"
msgstr "Mappenavn"

#. i18n: ectx: property (text), widget (QLabel, label)
#. +> trunk5 stable5
#: settingsdialog.ui:34
msgid "&Directory:"
msgstr "&Mappe:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#. +> trunk5 stable5
#: settingsdialog.ui:49
msgid "Select the directory whose contents should be represented by this resource. If the directory does not exist, it will be created."
msgstr "Velg den mappa som skal ha sitt innhold representert ved denne ressursen. Hvis mappa ikke finnes, blir den opprettet."

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#. +> trunk5 stable5
#: settingsdialog.ui:62 singlefileresourceconfigdialog_desktop.ui:106
msgid "Access Rights"
msgstr "Tilgangsretter"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ReadOnly)
#. +> trunk5 stable5
#: settingsdialog.ui:68 singlefileresourceconfigdialog_desktop.ui:112
msgid "Read only"
msgstr "Bare lesing"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#. +> trunk5 stable5
#: settingsdialog.ui:75
msgid "If read-only mode is enabled, no changes will be written to the directory selected above. Read-only mode will be automatically enabled if you do not have write access to the directory."
msgstr "Hvis \"Bare lesing\" er slått på, så blir ingen endringer skrevet til mappa valgt over. Skrivebeskyttelse blir automatisk slått på hvis du ikke har skriverettighet til mappa."

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#. +> trunk5 stable5
#: settingsdialog.ui:102
msgid "Tuning"
msgstr "Trimming"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#. +> trunk5 stable5
#: settingsdialog.ui:108
msgid "The options on this page allow you to change parameters that balance data safety and consistency against performance. In general you should be careful with changing anything here, the defaults are good enough in most cases."
msgstr "Med valgene på denne sida kan du endre parametre som balanserer sikkerhet og stabilitet for data mot ytelse. Som regel bør du være varsom med å endre noe her, standardverdiene er gode nok i de fleste tilfeller."

#. i18n: ectx: property (text), widget (QLabel, autosaveLabel)
#. +> trunk5 stable5
#: settingsdialog.ui:120
msgid "Autosave delay:"
msgstr "Tid mellom hver autolagring:"

#. +> trunk5 stable5
#: singlefileresource.h:74
msgid "No file selected."
msgstr "Ingen fil valgt."

#. +> trunk5 stable5
#: singlefileresource.h:76
msgid "The resource not configured yet"
msgstr "Ressursen er ikke satt opp ennå"

#. +> trunk5 stable5
#: singlefileresource.h:110 singlefileresource.h:139 singlefileresource.h:230
#: singlefileresourcebase.cpp:273 singlefileresourcebase.cpp:287
msgctxt "@info:status"
msgid "Ready"
msgstr "Klar"

#. +> trunk5 stable5
#: singlefileresource.h:112
#, kde-format
msgid "Could not create file '%1'."
msgstr "Klarte ikke opprette fila «%1»."

#. +> trunk5 stable5
#: singlefileresource.h:126
#, kde-format
msgid "Could not read file '%1'"
msgstr "Kunne ikke lese fila «%1»"

#. +> trunk5 stable5
#: singlefileresource.h:143
msgid "Another download is still in progress."
msgstr "En annen nedlasting pågår ennå."

#. +> trunk5 stable5
#: singlefileresource.h:154 singlefileresource.h:247
msgid "Another file upload is still in progress."
msgstr "En annen filopplasting pågår ennå."

#. +> trunk5 stable5
#: singlefileresource.h:172
msgid "Downloading remote file."
msgstr "Laster ned fil fra nettverket."

#. +> trunk5 stable5
#: singlefileresource.h:192
#, kde-format
msgid "Trying to write to a read-only file: '%1'."
msgstr "Forsøker å skrive til en skrivebeskyttet fil: «%1»"

#. +> trunk5 stable5
#: singlefileresource.h:205
msgid "No file specified."
msgstr "Ingen fil oppgitt. "

#. +> trunk5 stable5
#: singlefileresource.h:236
msgid "A download is still in progress."
msgstr "En nedlasting pågår ennå."

#. +> trunk5 stable5
#: singlefileresource.h:279
msgid "Uploading cached file to remote location."
msgstr "Laster opp mellomlagret fil til nettverksplassering."

#. +> trunk5 stable5
#: singlefileresourcebase.cpp:240
#, kde-format
msgid "The file '%1' was changed on disk. As a precaution, a backup of its previous contents has been created at '%2'."
msgstr "Fila «%1» ble endret på disken. En sikkerhetskopi av de tidligere endringene er lagret på «%2» for å unngå tap av data."

#. +> trunk5 stable5
#: singlefileresourcebase.cpp:263
#, kde-format
msgid "Could not load file '%1'."
msgstr "Kan ikke laste inn fila «%1»."

#. +> trunk5 stable5
#: singlefileresourcebase.cpp:279
#, kde-format
msgid "Could not save file '%1'."
msgstr "Klarte ikke lagre fila «%1»."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:21
msgid "File"
msgstr "Fil"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:27
msgid "Filename"
msgstr "Filnavn"

#. i18n: ectx: property (text), widget (QLabel, label)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:35
msgid "&Filename:"
msgstr "&Filnavn:"

#. i18n: ectx: property (text), widget (QLabel, statusLabel)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:50
msgid "Status:"
msgstr "Status:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:57
msgid "Select the file whose contents should be represented by this resource. If the file does not exist, it will be created. A URL of a remote file can also be specified, but note that monitoring for file changes will not work in this case."
msgstr "Velg den fila som skal få sitt innhold representert ved denne ressursen. Fila blir opprettet hvis den ikke finnes. Det kan også oppgis en URL til en nettverksfil, men merk at i så fall virker det ikke å overvåke fila etter endringer."

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_3)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:70
msgid "Display Name"
msgstr "Vist navn"

#. i18n: ectx: property (text), widget (QLabel, label_1)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:78
msgid "&Name:"
msgstr "&Navn:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:93
msgid "Enter the name used to identify this resource in displays. If not specified, the filename will be used."
msgstr "Oppgi navnet som brukes til å identifisere denne ressursen i visninger. Hvis utelatt blir filnavnet brukt."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:119
msgid "If read-only mode is enabled, no changes will be written to the file selected above. Read-only mode will be automatically enabled if you do not have write access to the file or the file is on a remote server that does not support write access."
msgstr "Hvis fila bare kan leses, blir det ikke skrevet noen endringer inn i fila som er valgt ovenfor. \"Bare lesing\" blir automatisk slått på hvis du ikke har skrivetilgang til fila, eller om den er på en nettverkstjener som ikke støtter skrivetilgang."

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_MonitorFile)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:132
msgid "Monitoring"
msgstr "Overvåker"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:138
msgid "If file monitoring is enabled the resource will reload the file when changes are made by other programs. It also tries to create a backup in case of conflicts whenever possible."
msgstr "Hvis filovervåking er slått på vil ressursen laste fila på nytt når andre programmer endrer den. Den forsøker også å lage en sikkerhetskopi hvis mulig i tilfelle konflikter."

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_MonitorFile)
#. +> trunk5 stable5
#: singlefileresourceconfigdialog_desktop.ui:148
msgid "Enable file &monitoring"
msgstr "Slå på fil&overvåking"

#. +> trunk5 stable5
#: singlefileresourceconfigdialogbase.cpp:178
msgctxt "@info:status"
msgid "Checking file information..."
msgstr "Kontrollerer filinformasjon …"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#. +> stable5
#: singlefileresourceconfigdialog_mobile.ui:43
msgid "&Display name:"
msgstr "&Vist navn:"

#~ msgid " minute"
#~ msgid_plural " minutes"
#~ msgstr[0] " minutt"
#~ msgstr[1] " minutter"

#~ msgid "The file '%1' was changed on disk while there were still pending changes in Akonadi. To avoid data loss, a backup of the internal changes has been created at '%2'."
#~ msgstr "Fila «%1» ble endret på disken mens Akonadi ennå hadde ventende endringer. En sikkerhetskopi av de interne endringene er lagret på «%2» for å unngå tap av data."

# Translation of audiocd-kio.desktop to Norwegian Bokmål
#
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 2003.
# Knut Yrvin <knut.yrvin@gmail.com>, 2003, 2004.
# Jørgen Grønlund <jorgenhg@broadpark.no>, 2004.
# Axel Bojer <axel@bojer.no>, 2005.
# Nils Kristian Tomren <slx@nilsk.net>, 2005, 2007.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009, 2010, 2011, 2012.
msgid ""
msgstr ""
"Project-Id-Version: desktop_kdemultimedia\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-12-27 10:43+0100\n"
"PO-Revision-Date: 2012-12-08 20:20+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk stable
#: data/audiocd.desktop:5
msgctxt "Name"
msgid "Audio CD Browser"
msgstr "Bla gjennom lyd-CD"

#. +> trunk stable
#: data/solid_audiocd.desktop:10
msgctxt "Name"
msgid "Open with File Manager"
msgstr "Åpne med filbehandler"

#. +> trunk stable
#: kcmaudiocd/audiocd.desktop:11
msgctxt "Name"
msgid "Audio CDs"
msgstr "Lyd-CD-er"

#. +> trunk stable
#: kcmaudiocd/audiocd.desktop:62
msgctxt "Comment"
msgid "Audiocd IO Slave Configuration"
msgstr "Oppsett av lyd-CD IO-slave"

#. +> trunk stable
#: kcmaudiocd/audiocd.desktop:111
msgctxt "X-KDE-Keywords"
msgid "Audio CD,CD,Ogg,Vorbis,Encoding,CDDA,Bitrate"
msgstr "Audio CD,CD,Ogg,Vorbis,Koding,CDDA,Bitrate"

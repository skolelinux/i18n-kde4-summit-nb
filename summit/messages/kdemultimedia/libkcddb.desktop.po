# Translation of libkcddb.desktop to Norwegian Bokmål
#
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 2003.
# Knut Yrvin <knut.yrvin@gmail.com>, 2003, 2004.
# Jørgen Grønlund <jorgenhg@broadpark.no>, 2004.
# Axel Bojer <axel@bojer.no>, 2005.
# Nils Kristian Tomren <slx@nilsk.net>, 2005, 2007.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009, 2010, 2011, 2012, 2015.
msgid ""
msgstr ""
"Project-Id-Version: desktop_kdemultimedia\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-12-27 10:43+0100\n"
"PO-Revision-Date: 2015-04-26 22:17+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk stable
#: kcmcddb/libkcddb.desktop:12
msgctxt "Name"
msgid "CDDB Retrieval"
msgstr "CDDB-henting"

#. +> trunk stable
#: kcmcddb/libkcddb.desktop:61
msgctxt "GenericName"
msgid "CDDB Configuration"
msgstr "CDDB-oppsett"

#. +> trunk stable
#: kcmcddb/libkcddb.desktop:111
msgctxt "Comment"
msgid "Configure the CDDB Retrieval"
msgstr "Oppsett av CDDB-henting"

#. +> trunk stable
#: kcmcddb/libkcddb.desktop:159
msgctxt "Keywords"
msgid "cddb;"
msgstr "cddb;"

#~ msgctxt "Keywords"
#~ msgid "cddb"
#~ msgstr "cddb"

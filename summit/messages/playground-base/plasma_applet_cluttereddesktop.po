# Translation of plasma_applet_cluttereddesktop to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2011-08-02 09:03+0200\n"
"PO-Revision-Date: 2011-03-25 09:34+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 1.1\n"

#. +> trunk
#: cluttereddesktop.cpp:67 desktop.cpp:397
msgid "Visible"
msgstr "Synlig"

#. +> trunk
#: cluttereddesktop.cpp:67 desktop.cpp:397
msgid "Hidden"
msgstr "Skjult"

#. +> trunk
#: cluttereddesktop.cpp:67 desktop.cpp:397
msgid "Not Found"
msgstr "Ikke funnet"

#. +> trunk
#: cluttereddesktop.cpp:71 desktop.cpp:401
msgid "ID"
msgstr "ID"

#. +> trunk
#: cluttereddesktop.cpp:72 desktop.cpp:402
msgid "Screen"
msgstr "Skjerm"

#. +> trunk
#: cluttereddesktop.cpp:73 desktop.cpp:403
msgid "Geometry"
msgstr "Geometri"

#. +> trunk
#: cluttereddesktop.cpp:76 desktop.cpp:406
msgid "Wallpaper"
msgstr "Tapet"

#. +> trunk
#: cluttereddesktop.cpp:78 desktop.cpp:408
msgid "Wallpaper mode"
msgstr "Tapetmodus"

#. +> trunk
#: cluttereddesktop.cpp:80 desktop.cpp:410
msgid "Toolbox"
msgstr "Verktøykasse"

#. +> trunk
#: cluttereddesktop.cpp:137 desktop.cpp:467
msgid "Desktop"
msgstr "Skrivebord"

#. i18n: ectx: property (text), widget (QCheckBox, cashewCheckBox)
#. +> trunk
#: config.ui:16
msgid "Show &Cashew"
msgstr "Vis &cashew"

#. i18n: ectx: property (text), widget (QCheckBox, debugCheckBox)
#. +> trunk
#: config.ui:23
msgid "Show &Plasma information"
msgstr "Vis &plasmainformasjon"

#. +> trunk
#: desktop.cpp:193
msgid "Add Panel"
msgstr "Legg til panel"

#. +> trunk
#: desktop.cpp:197
msgid "Run Command..."
msgstr "Kjør kommando …"

#. +> trunk
#: desktop.cpp:204
msgid "Lock Screen"
msgstr "Lås skjermen"

#. +> trunk
#: desktop.cpp:208
msgid "Leave..."
msgstr "Gå ut …"

# Translation of wicd-kde to Norwegian Bokmål
#
# Bjørn Kvisli <bjorn.kvisli@gmail.com>, 2011.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2011, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-03-28 09:51+0100\n"
"PO-Revision-Date: 2012-01-05 20:48+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Kvisli"

#. +> trunk
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjorn.kvisli@gmail.com"

#. +> trunk
#: applet/adhocdialog.cpp:30 applet/wicdapplet.cpp:158
msgid "Create an ad-hoc network"
msgstr "Lag et ad-hoc nettverk"

#. +> trunk
#: applet/adhocdialog.cpp:35 applet/infodialog.cpp:46
msgid "ESSID:"
msgstr "ESSID:"

#. +> trunk
#: applet/adhocdialog.cpp:36
msgctxt "Template name for an ad-hoc network"
msgid "My_Adhoc_Network"
msgstr "Mitt_adhoc_nettverk"

#. +> trunk
#: applet/adhocdialog.cpp:37 applet/infodialog.cpp:42 applet/infodialog.cpp:48
#: applet/properties/networkpropertiesdialog.cpp:43
msgid "IP:"
msgstr "IP:"

#. +> trunk
#: applet/adhocdialog.cpp:39 applet/wirelessnetworkitem.cpp:114
msgid "Channel:"
msgstr "Kanal:"

#. +> trunk
#: applet/adhocdialog.cpp:41
msgid "Activate Internet Connection Sharing"
msgstr "Slå på deling av internettforbindelsen"

#. +> trunk
#: applet/adhocdialog.cpp:43
msgid "Use Encryption (WEP only)"
msgstr "Bruk kryptering (kun WEP)"

#. +> trunk
#: applet/adhocdialog.cpp:44
msgid "Key:"
msgstr "Nøkkel:"

#. +> trunk
#: applet/infodialog.cpp:38 applet/infodialog.cpp:57
msgid "State:"
msgstr "Tilstand:"

#. +> trunk
#: applet/infodialog.cpp:38
msgid "Connecting"
msgstr "Oppretter forbindelse"

#. +> trunk
#: applet/infodialog.cpp:41 applet/infodialog.cpp:45
msgid "Connection type:"
msgstr "Tilkoblingstype:"

#. +> trunk
#: applet/infodialog.cpp:41
msgctxt "Connection type"
msgid "Wired"
msgstr "Kablet"

#. +> trunk
#: applet/infodialog.cpp:45
msgctxt "Connection type"
msgid "Wireless"
msgstr "Trådløs:"

#. +> trunk
#: applet/infodialog.cpp:47
msgid "Speed:"
msgstr "Hastighet:"

#. +> trunk
#: applet/infodialog.cpp:54 applet/wirelessnetworkitem.cpp:98
msgid "Signal strength:"
msgstr "Signalstyrke:"

#. +> trunk
#: applet/infodialog.cpp:57 applet/wicdapplet.cpp:216 applet/wicdapplet.cpp:244
msgid "Disconnected"
msgstr "Forbindelse brutt"

#. +> trunk
#: applet/networkplotter.cpp:97 applet/networkplotter.cpp:98
msgid "KiB/s"
msgstr "KiB/s"

#. +> trunk
#: applet/profilewidget.cpp:39
msgid "Use as default profile"
msgstr "Bruk som standardprofil"

#. +> trunk
#: applet/profilewidget.cpp:47
msgid "Add a profile..."
msgstr "Legg til en profil …"

#. +> trunk
#: applet/profilewidget.cpp:52
msgid "Remove the profile"
msgstr "Fjern profilen"

#. +> trunk
#: applet/profilewidget.cpp:115
msgid "Add a profile"
msgstr "Legg til profil"

#. +> trunk
#: applet/profilewidget.cpp:116
msgid "New profile name:"
msgstr "Navn på ny profil:"

#. +> trunk
#: applet/profilewidget.cpp:153
#: applet/properties/networkpropertiesdialog.cpp:37
msgid "OK"
msgstr "OK"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:42
msgid "Use Static IPs"
msgstr "Bruk statiske IPer"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:44
msgid "Netmask:"
msgstr "Nettverksmaske:"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:45
msgid "Gateway:"
msgstr "Innfallsport:"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:46
msgid "Use Static DNS"
msgstr "Bruk statisk DNS"

#. i18n: ectx: property (text), widget (QCheckBox, useDNSBox)
#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:47 kcm/kcmwidget.ui:115
msgid "Use global DNS servers"
msgstr "Bruk globale DNS-tjenere"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:48 kcm/kcmwidget.ui:130
msgid "DNS domain:"
msgstr "DNS-domene:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:49 kcm/kcmwidget.ui:144
msgid "Search domain:"
msgstr "Domenesøk:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:50 kcm/kcmwidget.ui:158
msgid "DNS server 1:"
msgstr "1. DNS-tjener:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:51 kcm/kcmwidget.ui:172
msgid "DNS server 2:"
msgstr "2. DNS-tjener:"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:52 kcm/kcmwidget.ui:186
msgid "DNS server 3:"
msgstr "3. DNS-tjener;"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:53
msgid "DHCP Hostname:"
msgstr "DHCP vertsnavn:"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:58
msgid "Automatically connect to this network"
msgstr "Opprett forbindelse til dette nettverket automatisk"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:59
msgid "Use these settings for all networks sharing this ESSID"
msgstr "Bruk disse innstillingene for alle nettverk som deler denne ESSID"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:60
msgid "Use encryption"
msgstr "Bruk kryptering"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:64
msgid "Scripts"
msgstr "Skript"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:163
msgid "Invalid IP address entered."
msgstr "Det ble oppgitt ennugyldig IP-adresse."

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:200
msgid "Global DNS has not been enabled in general preferences."
msgstr "I de almene innstillingene er det ikke muliggjort bruk av global DNS."

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:239
#: applet/properties/networkpropertiesdialog.cpp:247
#, kde-format
msgid "Invalid address in %1"
msgstr "Ugyldig adresse i %1"

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:260
#, kde-format
msgid "%1 is required."
msgstr "%1 behøves."

#. +> trunk
#: applet/properties/networkpropertiesdialog.cpp:265
msgid "This network needs an encryption."
msgstr "Dette nettverket trenger kryptering."

#. +> trunk
#: applet/properties/scriptsdialog.cpp:45
#: applet/properties/scriptsdialog.cpp:99
#, kde-format
msgid "KAuth returned an error code: %1"
msgstr "KAuth gav tilbake feilkoden: %1"

#. +> trunk
#: applet/properties/scriptsdialog.cpp:51
msgid "Configure Scripts"
msgstr "Oppsett av skript"

#. +> trunk
#: applet/properties/scriptsdialog.cpp:62
msgid "Pre-connection script"
msgstr "Skript som kjøres før tilkobling"

#. +> trunk
#: applet/properties/scriptsdialog.cpp:63
msgid "Post-connection script"
msgstr "Skript som kjøres etter tilkobling"

#. +> trunk
#: applet/properties/scriptsdialog.cpp:64
msgid "Pre-disconnection script"
msgstr "Skript som kjøres før frakobling"

#. +> trunk
#: applet/properties/scriptsdialog.cpp:65
msgid "Post-disconnection script"
msgstr "Skript som kjøres etter frakobling"

#. +> trunk
#: applet/wicdapplet.cpp:78
msgid "Unable to load the Wicd data engine."
msgstr "Klarte ikke å laste inn Wicds datamotor."

#. +> trunk
#: applet/wicdapplet.cpp:116
msgid "Abort"
msgstr "Abryt"

#. +> trunk
#: applet/wicdapplet.cpp:121 applet/wicdapplet.cpp:171
msgid "Reload"
msgstr "Last på nytt"

#. +> trunk
#: applet/wicdapplet.cpp:149 kcm/wicdkcm.cpp:43
msgid "Wicd Configuration"
msgstr "Oppsett av Wicd"

#. +> trunk
#: applet/wicdapplet.cpp:154
msgid "Connection information"
msgstr "Informasjon om tilkoblingen"

#. +> trunk
#: applet/wicdapplet.cpp:162 applet/wicdapplet.cpp:397
msgid "Find a hidden network"
msgstr "Finn et skjult nettverk"

#. +> trunk
#: applet/wicdapplet.cpp:166
#, fuzzy
#| msgid "Kill"
msgid "RfKill"
msgstr "Drep"

#. +> trunk
#: applet/wicdapplet.cpp:210
msgid "Connected to wired network"
msgstr "Forbunde med kablet nettverk"

#. +> trunk
#: applet/wicdapplet.cpp:213
msgid "Connected to wireless network"
msgstr "Forbundet med trådløst nettverk"

#. +> trunk
#: applet/wicdapplet.cpp:227
#, kde-format
msgid "Connected to wired network (IP: %1)"
msgstr "Forbundet med kablet nettverk (IP: %1)"

#. +> trunk
#: applet/wicdapplet.cpp:235
#, kde-format
msgid "Connected to %1 - %2%3 (IP: %4)"
msgstr "Forbundet med %1 – %2%3 (IP: %4)"

#. +> trunk
#: applet/wicdapplet.cpp:241
msgid "Wired network: "
msgstr "Kablet nettverk:"

#. +> trunk
#: applet/wicdapplet.cpp:251
msgid "The Wicd daemon is not running."
msgstr "Wicd-daemonen kjører ikke."

#. +> trunk
#: applet/wicdapplet.cpp:398
msgid "Hidden Network ESSID"
msgstr "ESSID for skjult nettverk"

#. +> trunk
#: applet/wicdapplet.cpp:462
msgid "General"
msgstr "Generelt"

#. i18n: ectx: property (text), widget (QCheckBox, displayqualityBox)
#. +> trunk
#: applet/wicdappletConfig.ui:17
msgid "Show signal strength"
msgstr "Vis signalstyrke"

#. i18n: ectx: property (text), widget (QCheckBox, autoscanBox)
#. +> trunk
#: applet/wicdappletConfig.ui:24
msgid "Automatically scan on open"
msgstr "Automatisk skanning ved åpning"

#. i18n: ectx: property (text), widget (QCheckBox, plotterBox)
#. +> trunk
#: applet/wicdappletConfig.ui:31
msgid "Show network traffic"
msgstr "Vis nettverkstrafikk"

#. +> trunk
#: applet/wirelessnetworkitem.cpp:104
msgid "Unsecured"
msgstr "Usikret"

#. +> trunk
#: applet/wirelessnetworkitem.cpp:105
msgid "Encryption type:"
msgstr "Krypteringstype:"

#. +> trunk
#: applet/wirelessnetworkitem.cpp:108
msgid "Access point address:"
msgstr "Tilgangspunktets adresse:"

#. +> trunk
#: applet/wirelessnetworkitem.cpp:111
msgid "Mode:"
msgstr "Måte:"

#. +> trunk
#: dataengine/wicdengine.cpp:40
msgid "Putting interface down..."
msgstr "Lukker grenseflate …"

#. +> trunk
#: dataengine/wicdengine.cpp:41
msgid "Resetting IP address..."
msgstr "Fastsetter IP-adressen igjen …"

#. +> trunk
#: dataengine/wicdengine.cpp:42
msgid "Putting interface up..."
msgstr "Åpner grenseflaten ...."

#. +> trunk
#: dataengine/wicdengine.cpp:43
msgid "Generating PSK..."
msgstr "Genererer PSK …"

#. +> trunk
#: dataengine/wicdengine.cpp:44
msgid "Connection Failed: Bad password."
msgstr "Fikk ikke forbindelse: Galt passord."

#. +> trunk
#: dataengine/wicdengine.cpp:45
msgid "Generating WPA configuration"
msgstr "Genererer WPA-oppsett"

#. +> trunk
#: dataengine/wicdengine.cpp:46
msgid "Validating authentication..."
msgstr "Kontrollerer identiteten …"

#. +> trunk
#: dataengine/wicdengine.cpp:47
msgid "Obtaining IP address..."
msgstr "Får tak i IP-adresse …"

#. +> trunk
#: dataengine/wicdengine.cpp:48
msgid "Done connecting..."
msgstr "Forbindelse oppnådd …"

#. +> trunk
#: dataengine/wicdengine.cpp:49
msgid "Connection Failed: Unable to Get IP Address"
msgstr "Fikk ikke forbindelse: Klarte ikke å få tak i IP-adresse"

#. +> trunk
#: dataengine/wicdengine.cpp:50
msgid "Connection Failed: No DHCP offers received."
msgstr "Fikk ikke forbindelse: Fikk ingen tilbud om DHCP"

#. +> trunk
#: dataengine/wicdengine.cpp:51
msgid "Verifying access point association..."
msgstr "Bekrefter tilhørighet til tilgangspunkt …"

#. +> trunk
#: dataengine/wicdengine.cpp:52
msgid "Connection failed: Could not contact the wireless access point."
msgstr "Fikk ikke forbindelse: Klarte ikke å få kontakt med det trådløse tilgangspunktet."

#. +> trunk
#: dataengine/wicdengine.cpp:53
msgid "Setting static IP addresses..."
msgstr "Setter statisk IP-adresse …"

#. +> trunk
#: dataengine/wicdengine.cpp:54
msgid "Aborted"
msgstr "Avbrutt"

#. +> trunk
#: dataengine/wicdengine.cpp:55
msgid "Failed"
msgstr "Mislyktes"

#. +> trunk
#: dbushandler.cpp:171
msgid "Wired network"
msgstr "Kablet nettverk"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#. +> trunk
#: kcm/kcmwidget.ui:24
msgid "General settings"
msgstr "Almene innstillinger"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#. +> trunk
#: kcm/kcmwidget.ui:30
msgid "Network interfaces"
msgstr "Grenseflater til nettverket"

#. i18n: ectx: property (text), widget (QLabel, label)
#. +> trunk
#: kcm/kcmwidget.ui:41
msgid "Wireless interface:"
msgstr "Trådløs grenseflate:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#. +> trunk
#: kcm/kcmwidget.ui:48
msgid "Wired interface:"
msgstr "Kablet grenseflate:"

#. i18n: ectx: property (text), widget (QCheckBox, showWiredBox)
#. +> trunk
#: kcm/kcmwidget.ui:92
msgid "Always show wired interface"
msgstr "Vis alltid kablet grenesflate"

#. i18n: ectx: property (text), widget (QCheckBox, swithcWiredBox)
#. +> trunk
#: kcm/kcmwidget.ui:99
msgid "Always switch to a wired connection when available"
msgstr "Bytt alltid til kablet tilkobling når denne foreligger"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#. +> trunk
#: kcm/kcmwidget.ui:109
msgid "Global DNS servers"
msgstr "Globale DNS-tjenere"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_3)
#. +> trunk
#: kcm/kcmwidget.ui:205
msgid "Other"
msgstr "Annet"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#. +> trunk
#: kcm/kcmwidget.ui:213
msgid "Wired automatic connection:"
msgstr "Automatisk kablet tilkobling:"

#. i18n: ectx: property (text), widget (QCheckBox, reconnectBox)
#. +> trunk
#: kcm/kcmwidget.ui:238
msgid "Automatically reconnect on network connection loss"
msgstr "Bygg opp ny forbindelse til nettverket automatisk dersom tilkoblingen brytes"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#. +> trunk
#: kcm/kcmwidget.ui:262
msgid "External programs"
msgstr "Eksterne programmer"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#. +> trunk
#: kcm/kcmwidget.ui:271
msgid "Route table flushing:"
msgstr "Tømming av rutetabell:"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#. +> trunk
#: kcm/kcmwidget.ui:281
msgid "Wired link detection:"
msgstr "Gjenkjenning av link:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#. +> trunk
#: kcm/kcmwidget.ui:291
msgid "DHCP client:"
msgstr "DHCP-klient:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#. +> trunk
#: kcm/kcmwidget.ui:302
msgid "Advanced settings"
msgstr "Avansert oppsett"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_5)
#. +> trunk
#: kcm/kcmwidget.ui:308
msgid "Miscellaneous"
msgstr "Forskjellig"

#. i18n: ectx: property (text), widget (QLabel, label_15)
#. +> trunk
#: kcm/kcmwidget.ui:326
msgid "WPA Supplicant driver:"
msgstr "WPA Supplicant driver:"

#. i18n: ectx: property (text), widget (QLabel, label_16)
#. +> trunk
#: kcm/kcmwidget.ui:352
msgid "Backend:"
msgstr "Bakstykke:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_7)
#. +> trunk
#: kcm/kcmwidget.ui:375
msgid "Debugging"
msgstr "Feilsøking"

#. i18n: ectx: property (text), widget (QCheckBox, debugBox)
#. +> trunk
#: kcm/kcmwidget.ui:381
msgid "Enable debug mode"
msgstr "Tillat feilsøking"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_8)
#. +> trunk
#: kcm/kcmwidget.ui:391
msgid "Wireless interface"
msgstr "Trådløs grenseflate"

#. i18n: ectx: property (text), widget (QCheckBox, dbmBox)
#. +> trunk
#: kcm/kcmwidget.ui:397
msgid "Use dBm to measure signal strength"
msgstr "Bruk dBm til å måle signalstyrke"

#. i18n: ectx: property (text), widget (QCheckBox, checkBox)
#. +> trunk
#: kcm/kcmwidget.ui:404
msgid "Ping static gateways after connecting to verify association"
msgstr "Ping statiske innfallsporter etter at forbindelsen er oppnådd for å bekrefte tilhørighet"

#. +> trunk
#: kcm/wicdkcm.cpp:43
msgid "kcmwicd"
msgstr "kcmwicd"

#. +> trunk
#: kcm/wicdkcm.cpp:45
msgid "(c) 2009 Dario Freddi"
msgstr "© 2009 Dario Freddi"

#. +> trunk
#: kcm/wicdkcm.cpp:47
msgid "Dario Freddi"
msgstr "Dario Freddi"

#. +> trunk
#: kcm/wicdkcm.cpp:47
msgid "Original author"
msgstr "Opprinnelig forfatter"

#. +> trunk
#: kcm/wicdkcm.cpp:48
msgid "Anthony Vital"
msgstr "Anthony Vital"

#. +> trunk
#: kcm/wicdkcm.cpp:48
msgid "Developer"
msgstr "Utvikler"

#. +> trunk
#: kcm/wicdkcm.cpp:76 kcm/wicdkcm.cpp:104 kcm/wicdkcm.cpp:115
#: kcm/wicdkcm.cpp:126
msgid "Automatic (recommended)"
msgstr "Automatisk (anbefalt)"

#. +> trunk
#: kcm/wicdkcm.cpp:137
msgid "Use default wired network profile"
msgstr "Bruk standardprofil for kablet nettverk"

#. +> trunk
#: kcm/wicdkcm.cpp:138
msgid "Prompt for wired network profile"
msgstr "Spør etter profil for kablet nettverk"

#. +> trunk
#: kcm/wicdkcm.cpp:139
msgid "Use last wired network profile"
msgstr "Bruk forrige profil for kablet nettverk"

#~ msgid "Ok"
#~ msgstr "Ok"

#~ msgid "Form"
#~ msgstr "Skjema"

#~ msgid "Network information"
#~ msgstr "Informasjon om nettverket"

#~ msgid "Client settings"
#~ msgstr "Oppsett for klient"

#~ msgid "Show tooltips"
#~ msgstr "Vis hjelpebobler"

#~ msgid "Wicd Client KDE"
#~ msgstr "Wicd KDE-klient"

#~ msgid "Wicd client for KDE"
#~ msgstr "Wicd-klient for KDE"

#~ msgid "(c) 2010 Anthony Vital"
#~ msgstr "© 2010 Anthony Vital"

#~ msgid "Author"
#~ msgstr "Forfatter"

#~ msgid "Contributor and original KCM author"
#~ msgstr "Bidragsyter og opprinnelig forfatter av KCM"

#~ msgid "Brieuc Roblin"
#~ msgstr "Brieuc Roblin"

#~ msgid "Contributor"
#~ msgstr "Bidragsyter"

#~ msgid "Yoann Laissus"
#~ msgstr "Yoann Laissus"

#~ msgid "Preferences"
#~ msgstr "Innstillinger"

#~ msgid "Disconnect"
#~ msgstr "Bryt forbindelsen"

#~ msgid "Connect"
#~ msgstr "Lag forbindelse"

#~ msgid "Properties"
#~ msgstr "Egenskaper"

#~ msgid "Information"
#~ msgstr "Infromasjon"

#~ msgid "Manage profiles"
#~ msgstr "Behandle profiler"

#~ msgid "Manage Profiles"
#~ msgstr "Behandle profiler"

#~ msgid "Use as default profile (overwrites any previous default)"
#~ msgstr "Bruk som standardprofil (overskriver tidligere standardinnstilling)"

#~ msgid "&File"
#~ msgstr "&Fil"

#~ msgid "&Network"
#~ msgstr "&Nettverk"

#~ msgid "&Settings"
#~ msgstr "&Innstillinger"

#~ msgid "&Help"
#~ msgstr "&Hjelp"

#~ msgid "Main Toolbar"
#~ msgstr "Hovedverktøylinje"

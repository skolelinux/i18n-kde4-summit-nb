# Translation of org.kde.kmail.appdata to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2015-07-27 13:45+0200\n"
"PO-Revision-Date: 2014-09-13 14:56+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. (itstool) path: component/name
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:6
msgid "KMail"
msgstr "KMail"

#. (itstool) path: description/p
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:8
msgid "KMail is the email component of Kontact, the integrated personal information manager from KDE."
msgstr "KMail er e-postkomponenten i Kontact, KDEs integrerte personlige informasjonsbehandler."

#. (itstool) path: description/p
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:11
msgid "Features:"
msgstr "Egenskaper:"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:13
msgid "Supports the standard mail protocols IMAP, POP3 and SMTP."
msgstr "Støtter standard e-post-protokollene IMAP, POP3 og SMTP."

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:14
msgid "Supports authentication via NTLM (Microsoft Windows) and GSSAPI (Kerberos)"
msgstr "Støtter autentisering via NTLM (Microsoft Windows) og GSSAPI (Kerberos)"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:15
msgid "Supports plain text and secure logins, using SSL and TLS."
msgstr "Støtter innlogging med ren tekst og sikker innlogging med SSL og TLS."

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:16
msgid "Integration of international character sets and spell-checking (as-you-type and on demand)"
msgstr "Internasjonale tegnsett og stavekontroll er integrert. Stavekontroll fortløpende eller på forespørsel."

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:17
msgid "Native support for inline OpenPGP, PGP/MIME, and S/MIME."
msgstr "Innebygget støtte for OpenPGP, PGP/MIME og S/MIME."

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:18
msgid "Reading and writing of HTML mail."
msgstr "Lese og skrive HTML e-post."

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:19
msgid "Ability to display plain text only from an HTML mail."
msgstr "Kan vise bare ren tekst når posten er HTML."

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:20
msgid "Allows you to receive and accept invitations."
msgstr "Kan motta og akseptere invitasjoner."

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:21
msgid "Integration with popular spam checkers, e.g. SpamAssassin, Bogofilter, etc."
msgstr "Integrering med populære spamsjekkere, f.eks. SpamAssassin, Bogofilter osv."

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:22
msgid "An optional spam probability meter can be displayed"
msgstr "Kan vise, om ønskes, sannsynlighet for spam"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:23
msgid "Powerful search and filter abilities"
msgstr "Slagkraftig søk- og filter-muligheter"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:24
msgid "Can import mail from many other clients"
msgstr "Kan importere e-post fra mange andre klienter"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:25
msgid "Searching in IMAP folders fully supported"
msgstr "Full støtte for søking i IMAP-mapper"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:26
msgid "Highly integrated with other Kontact components"
msgstr "Sterkt integrert med andre Kontact-komponenter"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:27
msgid "Encrypted password saving in KWallet"
msgstr "Lagrer krypterte passord i KWallet"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:28
msgid "Flagging and tagging of messages to aid sorting and recovery of information"
msgstr "Flagger og setter etikett på meldinger for enklere sortering og informasjonshenting"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:29
msgid "Supports mailing list management features"
msgstr "Støtter funksjoner for e-postlister"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:30
msgid "Can replace text smilies with emoticons"
msgstr "Kan erstatte tekst-smilefjes med emotikoner"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:31
msgid "Displays the sender's picture or avatar if present in the address book"
msgstr "Viser avsenders bilde eller avatar hvis det finnes i adresseboka"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:32
msgid "Supports X-Face (b/w pictures in messages)"
msgstr "Støtter X-Face (s/hv bilder i meldinger)"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:33
msgid "Compression of attachments"
msgstr "Komprimerer vedlegg        "

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.kmail.appdata.xml:34
msgid "KMail supports Groupware functionality."
msgstr "KMail støtter Gruppevarefunksjonalitet."

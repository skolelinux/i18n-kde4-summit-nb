# Translation of kdevduchainviewer to Norwegian Bokmål
#
msgid ""
msgstr ""
"Project-Id-Version: KDE 4\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2011-08-02 09:03+0200\n"
"PO-Revision-Date: 2008-11-26 17:46+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk
#, fuzzy
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Steensrud"

#. +> trunk
#, fuzzy
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjornst@skogkatt.homelinux.org"

#. +> trunk
#: duchainmodel.cpp:232
#, fuzzy, kde-format
#| msgctxt "Date range: monthStart dayStart - monthEnd dayEnd"
#| msgid "%1 %2 - %3 %4"
msgid "Range: %1:%2 -> %3 %4"
msgstr "%2 %1 – %4 %3"

#. +> trunk
#: duchainmodel.cpp:238
#, fuzzy, kde-format
msgid "Imported Context: %1"
msgstr "Kontekst: %1"

#. +> trunk
#: duchainmodel.cpp:240
#, fuzzy
#| msgid "Top level"
msgid "Top level context"
msgstr "1 brett"

#. +> trunk
#: duchainmodel.cpp:242
#, fuzzy, kde-format
msgid "Context: %1"
msgstr "Kontekst: %1"

#. +> trunk
#: duchainmodel.cpp:249
#, fuzzy, kde-format
#| msgid "Definition:"
msgid "Definition: %1"
msgstr "Definisjon:"

#. +> trunk
#: duchainmodel.cpp:251
#, fuzzy, kde-format
#| msgid "Declaration"
msgid "Declaration: %1"
msgstr "Deklarasjon"

#. +> trunk
#: duchainmodel.cpp:264
#, fuzzy
#| msgid "Unknown object %1"
msgid "Unknown object."
msgstr "Ukjent objekt %1"

#. +> trunk
#: duchainmodel.cpp:546
#, fuzzy, kde-format
#| msgid "Cannot create file %1: %2"
msgid "Cannot create temporary file \"%1\" with suffix \"%2\""
msgstr "Klarte ikke opprette fila %1:  %2"

#. +> trunk
#: duchainmodel.cpp:564
#, kde-format
msgid "Could not open %1 with kgraphviewer or dotty."
msgstr ""

#. +> trunk
#: duchaintree.cpp:46
#, fuzzy
#| msgid "Definition"
msgid "Definition-Use Chain"
msgstr "Definisjon"

#. +> trunk
#: duchainviewplugin.cpp:37
#, fuzzy
#| msgid "Main View"
msgid "DUChain View"
msgstr "Hovedvisning"

#. +> trunk
#: duchainviewplugin.cpp:37
msgid "A simple tool to view the raw DUChain"
msgstr ""

#. +> trunk
#: duchainviewplugin.cpp:71
msgid "DUChain Viewer"
msgstr ""

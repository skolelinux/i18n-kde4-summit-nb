# Translation of kdevmercurial to Norwegian Bokmål
#
msgid ""
msgstr ""
"Project-Id-Version: KDE 4\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-04-12 10:00+0200\n"
"PO-Revision-Date: 2008-11-26 17:46+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk
#, fuzzy
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Steensrud"

#. +> trunk
#, fuzzy
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjornst@skogkatt.homelinux.org"

#. i18n: ectx: Menu (dvcs_popup)
#. +> trunk
#: kdevmercurial.rc:5
#, fuzzy
#| msgctxt "Name"
#| msgid "Mercurial"
msgctxt "@title:menu"
msgid "Mercurial"
msgstr "Mercurial"

#. +> trunk
#: mercurialplugin.cpp:58
#, fuzzy
#| msgctxt "Name"
#| msgid "Mercurial"
msgid "Mercurial"
msgstr "Mercurial"

#. +> trunk
#: mercurialplugin.cpp:58
msgid "A plugin to support Mercurial version control systems"
msgstr ""

#. +> trunk
#: mercurialplugin.cpp:69
#, fuzzy
#| msgctxt "@label device"
#| msgid "Heads:"
msgid "Heads..."
msgstr "Hoder:"

#. +> trunk
#: mercurialplugin.cpp:70
#, fuzzy
#| msgid "&New..."
msgctxt "mercurial queues submenu"
msgid "New..."
msgstr "&Ny …"

#. +> trunk
#: mercurialplugin.cpp:71
#, fuzzy
#| msgctxt "Name"
#| msgid "Pushto"
msgctxt "mercurial queues submenu"
msgid "Push"
msgstr "Pushto"

#. +> trunk
#: mercurialplugin.cpp:72
#, fuzzy
#| msgid "Pause All"
msgctxt "mercurial queues submenu"
msgid "Push All"
msgstr "Pause alle"

#. +> trunk
#: mercurialplugin.cpp:73
#, fuzzy
#| msgid "JPop"
msgctxt "mercurial queues submenu"
msgid "Pop"
msgstr "Japansk Pop"

#. +> trunk
#: mercurialplugin.cpp:74
#, fuzzy
#| msgid "Stop All"
msgctxt "mercurial queues submenu"
msgid "Pop All"
msgstr "Stopp alle"

#. +> trunk
#: mercurialplugin.cpp:75
#, fuzzy
#| msgid "&Manager"
msgctxt "mercurial queues submenu"
msgid "Manager..."
msgstr "&Leder"

#. +> trunk
#: mercurialplugin.cpp:1070
#, fuzzy
#| msgctxt "Name"
#| msgid "Mercurial"
msgid "Mercurial Queues"
msgstr "Mercurial"

#. +> trunk
#: mercurialpushjob.cpp:83
msgid "Enter your login and password for Mercurial push."
msgstr ""

#. +> trunk
#: mercurialpushjob.cpp:95 mercurialpushjob.cpp:99 mercurialpushjob.cpp:102
#: mercurialpushjob.cpp:105
#, fuzzy
#| msgid "Yourls Error"
msgid "Mercurial Push Error"
msgstr "Yourls-feil"

#. +> trunk
#: mercurialpushjob.cpp:95
msgid "Remote server does not accept your SSH key."
msgstr ""

#. +> trunk
#: mercurialpushjob.cpp:99
msgid "Remote server SSH fingerprint is unknown."
msgstr ""

#. +> trunk
#: mercurialpushjob.cpp:102
msgid "Push URL is incorrect."
msgstr ""

#. +> trunk
#: mercurialpushjob.cpp:105
msgid "Unknown error while pushing. Please, check Version Control toolview."
msgstr ""

#. +> trunk
#: ui/mercurialheadswidget.cpp:44
#, fuzzy, kde-format
#| msgctxt "Name"
#| msgid "Mercurial"
msgid "Mercurial Heads (%1)"
msgstr "Mercurial"

#. i18n: ectx: property (text), widget (QPushButton, checkoutPushButton)
#. +> trunk
#: ui/mercurialheadswidget.ui:44
#, fuzzy
msgid "Checkout"
msgstr "SJekk ut"

#. i18n: ectx: property (text), widget (QPushButton, mergePushButton)
#. +> trunk
#: ui/mercurialheadswidget.ui:51
msgid "Merge into Current"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, closePushButton)
#. +> trunk
#: ui/mercurialheadswidget.ui:71
#, fuzzy
msgid "Close"
msgstr "Lukk"

#. i18n: ectx: property (windowTitle), widget (QWidget, MercurialManagerWidget)
#. +> trunk
#: ui/mercurialqueuesmanager.ui:14
#, fuzzy
#| msgid "Queue Manager"
msgid "Mercurial Queues Manager"
msgstr "Købehandler"

#. i18n: ectx: property (text), widget (QPushButton, insertPushButton)
#. +> trunk
#: ui/mercurialqueuesmanager.ui:41
#, fuzzy
#| msgctxt "@action:inmenu Insert View"
#| msgid "Insert..."
msgid "Insert..."
msgstr "Sett inn …"

#. i18n: ectx: property (text), widget (QPushButton, pushPushButton)
#. +> trunk
#: ui/mercurialqueuesmanager.ui:61
#, fuzzy
#| msgctxt "Name"
#| msgid "Pushto"
msgid "Push"
msgstr "Pushto"

#. i18n: ectx: property (text), widget (QPushButton, pushAllPushButton)
#. +> trunk
#: ui/mercurialqueuesmanager.ui:68
#, fuzzy
#| msgid "Pause All"
msgid "Push All"
msgstr "Pause alle"

#. i18n: ectx: property (text), widget (QPushButton, popPushButton)
#. +> trunk
#: ui/mercurialqueuesmanager.ui:88
#, fuzzy
#| msgid "JPop"
msgid "Pop"
msgstr "Japansk Pop"

#. i18n: ectx: property (text), widget (QPushButton, popAllPushButton)
#. +> trunk
#: ui/mercurialqueuesmanager.ui:95
#, fuzzy
#| msgid "Stop All"
msgid "Pop All"
msgstr "Stopp alle"

#. i18n: ectx: property (text), widget (QPushButton, removePushButton)
#. +> trunk
#: ui/mercurialqueuesmanager.ui:118
msgid "Remove"
msgstr "Fjern"

# Translation of playground-artwork.desktop to Norwegian Bokmål
#
msgid ""
msgstr ""
"Project-Id-Version: desktop files\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-04-04 10:13+0200\n"
"PO-Revision-Date: 2007-09-11 22:44+0200\n"
"Last-Translator: MagicPO 0.3 (automated)\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MagicPO 0.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk
#: cokoon/decoration/cokoon.desktop:4
msgctxt "Name"
msgid "Cokoon Theme"
msgstr ""

#. +> trunk
#: cokoon/decoration/CokoonDecorationTheme.desktop:4
msgctxt "Comment"
msgid "A Cokoon KWin Decoration Theme"
msgstr ""

#. +> trunk
#: cokoon/decoration/TestTheme/TestTheme.desktop:2
#: cokoon/style/TestTheme/TestTheme.desktop:2
#, fuzzy
#| msgid "Test Theme"
msgctxt "Name"
msgid "TestTheme"
msgstr "Test tema"

#. +> trunk
#: cokoon/decoration/TestTheme/TestTheme.desktop:42
#, fuzzy
#| msgctxt "Comment"
#| msgid "Backend for Cantor for testing purposes"
msgctxt "Comment"
msgid "The very first CokoonDecoration theme, for testing purposes"
msgstr "Bakgrunnsmotor for Cantor for testing"

#. +> trunk
#: cokoon/style/cokoonstyle.themerc:2 plastik-focus/plastik.themerc:2
#: plastik/plastik.themerc:2
#, fuzzy
msgctxt "Name"
msgid "Plastik"
msgstr "Plastik"

#. +> trunk
#: cokoon/style/cokoonstyle.themerc:49 plastik-focus/plastik.themerc:49
#: plastik/plastik.themerc:49
msgctxt "Comment"
msgid "A simple and clean style"
msgstr ""

#. +> trunk
#: cokoon/style/CokoonStyleTheme.desktop:4
#, fuzzy
#| msgid "Clone Theme"
msgctxt "Comment"
msgid "A Cokoon Style Theme"
msgstr "Klon tema"

#. +> trunk
#: cokoon/style/TestTheme/TestTheme.desktop:42
#, fuzzy
#| msgctxt "Comment"
#| msgid "Backend for Cantor for testing purposes"
msgctxt "Comment"
msgid "The very first CokoonStyle theme, for testing purposes"
msgstr "Bakgrunnsmotor for Cantor for testing"

#. +> trunk
#: ion/ion.themerc:2
#, fuzzy
msgctxt "Name"
msgid "Ion"
msgstr "Ion"

#. +> trunk
#: ion/ion.themerc:50
msgctxt "Comment"
msgid "Soft, clean, modern look"
msgstr ""

#. +> trunk
#: KDEArtWork.port/IconThemes/kdeclassic/index.theme:2
#, fuzzy
msgctxt "Name"
msgid "KDE-Classic"
msgstr "KDE-Klassisk"

#. +> trunk
#: KDEArtWork.port/IconThemes/kdeclassic/index.theme:38
#, fuzzy
msgctxt "Comment"
msgid "KDE Classic Icon Theme"
msgstr "KDE Klassisk ikontema"

#. +> trunk
#: Oxygen/utils/index.theme:2
#, fuzzy
msgctxt "Name"
msgid "Oxygen"
msgstr "Oxygen"

#. +> trunk
#: Oxygen/utils/index.theme:47
#, fuzzy
#| msgctxt "Comment"
#| msgid "Oxygen Team - 2007"
msgctxt "Comment"
msgid "Oxygen Team - 2006"
msgstr "Oxygen Team – 2007"

#. +> trunk
#: smooth-blend/client/smoothblend.desktop:4
#, fuzzy
#| msgctxt "City in Indiana USA"
#| msgid "South Bend"
msgctxt "Name"
msgid "Smooth Blend"
msgstr "South Bend"

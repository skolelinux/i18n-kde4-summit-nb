# Translation of gwenview.appdata to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2015-01-23 22:55+0100\n"
"PO-Revision-Date: 2014-10-29 17:36+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. (itstool) path: component/name
#. +> trunk5 stable5
#: gwenview.appdata.xml:6
msgid "Gwenview"
msgstr "Gwenview"

#. (itstool) path: description/p
#. +> trunk5 stable5
#: gwenview.appdata.xml:8
msgid "Gwenview is a fast and easy to use image viewer for KDE."
msgstr "Gwenview er en rask bildeviser for KDE, som er lett å bruke."

#. (itstool) path: description/p
#. +> trunk5 stable5
#: gwenview.appdata.xml:11
msgid "Features:"
msgstr "Funksjoner:"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: gwenview.appdata.xml:13
msgid "Supports simple image manipulations: rotate, mirror, flip, and resize"
msgstr "Støtter enkle bildemanipuleringer: rotere, speilvende, snu om og endre størrelse"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: gwenview.appdata.xml:14
msgid "Supports basic file management actions such as copy, move, delete, and others"
msgstr "Støtter enkle filhandlinger som kopiere, flytte, slette og andre"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: gwenview.appdata.xml:15
msgid "Functions both as a standalone application and an embedded viewer in the Konqueror web browser"
msgstr "Virker både som et frittstående program og som innebygget viser i nettleseren Konqueror"

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: gwenview.appdata.xml:16
msgid "Can be extended using KIPI plugins."
msgstr "Kan utvides med KIPI programtillegg."

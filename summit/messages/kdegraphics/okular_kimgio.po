# Translation of okular_kimgio to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: okular_kimgio\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2013-10-21 09:35+0200\n"
"PO-Revision-Date: 2009-10-27 17:57+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk stable
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Steensrud"

#. +> trunk stable
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjornst@skogkatt.homelinux.org"

#. +> trunk stable
#: generator_kimgio.cpp:35
msgid "Image Backend"
msgstr "Bildemotor"

#. +> trunk stable
#: generator_kimgio.cpp:37
msgid "A simple image backend"
msgstr "En enkel bildemotor"

#. +> trunk stable
#: generator_kimgio.cpp:39
msgid ""
"© 2005, 2009 Albert Astals Cid\n"
"© 2006-2007 Pino Toscano\n"
"© 2006-2007 Tobias Koenig"
msgstr ""
"© 2005, 2009 Albert Astals Cid\n"
"© 2006-2007 Pino Toscano\n"
"© 2006-2007 Tobias Koenig"

#. +> trunk stable
#: generator_kimgio.cpp:43
msgid "Albert Astals Cid"
msgstr "Albert Astals Cid"

#. +> trunk stable
#: generator_kimgio.cpp:44
msgid "Pino Toscano"
msgstr "Pino Toscano"

#. +> trunk stable
#: generator_kimgio.cpp:45
msgid "Tobias Koenig"
msgstr "Tobias Koenig"

#. +> trunk stable
#: generator_kimgio.cpp:83 generator_kimgio.cpp:114
#, kde-format
msgid "Unable to load document: %1"
msgstr "Kan ikke laste inn dokumentet: %1"

#. i18n: ectx: ToolBar (mainToolBar)
#. +> trunk stable
#: gui.rc:3
msgid "Main Toolbar"
msgstr "Hovedverktøylinje"

# Translation of kactivities.desktop to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2011, 2012, 2013, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: desktop files\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-06-04 09:18+0200\n"
"PO-Revision-Date: 2015-04-28 08:54+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk5 stable
#: src/service/files/kactivitymanagerd-plugin.desktop:5
msgctxt "Comment"
msgid "Activity manager plugin"
msgstr "Programtillegg for aktivitetsbehandler"

#. +> trunk5 stable
#: src/service/files/kactivitymanagerd.desktop:9
msgctxt "Name"
msgid "Activity Manager"
msgstr "Aktivitetsbehandler"

#. +> trunk5 stable
#: src/service/files/kactivitymanagerd.desktop:63
msgctxt "Comment"
msgid "The activity management backend"
msgstr "Bakgrunnsmotoren for aktivitetsbehandling"

#. +> trunk5
#: src/service/plugins/activitytemplates/kactivitymanagerd-plugin-activitytemplates.desktop:2
msgctxt "Name"
msgid "Activity Templates"
msgstr "Film-maler"

#. +> trunk5
#: src/service/plugins/activitytemplates/kactivitymanagerd-plugin-activitytemplates.desktop:45
msgctxt "Comment"
msgid "Support for creating templated activities"
msgstr "Støtte for å opprette aktiviteter fra maler"

#. +> trunk5
#: src/service/plugins/eventspy/kactivitymanagerd-plugin-eventspy.desktop:2
msgctxt "Name"
msgid "Event Spy"
msgstr "Hendelsesovervåker"

#. +> trunk5
#: src/service/plugins/eventspy/kactivitymanagerd-plugin-eventspy.desktop:33
msgctxt "Comment"
msgid "Collects events from applications that support recent documents specification"
msgstr "Samler hendelser fra programmer som støtter spesifikasjon for nylig brukte dokumenter"

#. +> trunk5 stable
#: src/service/plugins/globalshortcuts/kactivitymanagerd-plugin-globalshortcuts.desktop:2
msgctxt "Name"
msgid "Global Shortcuts"
msgstr "Globale snarveier"

#. +> trunk5 stable
#: src/service/plugins/globalshortcuts/kactivitymanagerd-plugin-globalshortcuts.desktop:53
msgctxt "Comment"
msgid "Adds global keyboard shortcuts for activity switching"
msgstr "Legger til globale snarveistaster som bytter mellom aktiviteter"

#. +> trunk5 stable
#: src/service/plugins/slc/kactivitymanagerd-plugin-slc.desktop:2
msgctxt "Name"
msgid "Share-Like-Connect"
msgstr "Share-Like-Connect"

#. +> trunk5 stable
#: src/service/plugins/slc/kactivitymanagerd-plugin-slc.desktop:51
msgctxt "Comment"
msgid "Provides data to Share-Like-Connect applet"
msgstr "Skaffer data til miniprogrammet Share-Like-Connect"

#. +> trunk5 stable
#: src/service/plugins/sqlite/kactivitymanagerd-plugin-sqlite.desktop:2
msgctxt "Name"
msgid "Sqlite Feeder"
msgstr "Sqlite innmating"

#. +> trunk5 stable
#: src/service/plugins/sqlite/kactivitymanagerd-plugin-sqlite.desktop:52
msgctxt "Comment"
msgid "Plugin to store and score events in Sqlite"
msgstr "Programtillegg for å lagre og poengsette hendelser i Sqlite"

#. +> trunk5 stable
#: src/service/plugins/virtualdesktopswitch/kactivitymanagerd-plugin-virtualdesktopswitch.desktop:2
msgctxt "Name"
msgid "Virtual desktop switcher"
msgstr "Bytter virtuelle skrivebord"

#. +> trunk5 stable
#: src/service/plugins/virtualdesktopswitch/kactivitymanagerd-plugin-virtualdesktopswitch.desktop:53
msgctxt "Comment"
msgid "When switching to an activity, opens the virtual desktop last used with that activity"
msgstr "Når det byttes til en aktivitet, så åpnes det virtuelle skrivebordet som sist ble brukt med den aktiviteten"

#. +> trunk5 stable
#: src/workspace/fileitemplugin/kactivitymanagerd_fileitem_linking_plugin.desktop:4
msgctxt "Name"
msgid "File to activity linking plugin"
msgstr "Programtillegg som binder fil til aktivitet"

#. +> trunk5 stable
#: src/workspace/settings/kcm_activities.desktop:13
msgctxt "Name"
msgid "Activities"
msgstr "Aktiviteter"

#. +> trunk5 stable
#: src/workspace/settings/kcm_activities.desktop:64
msgctxt "Comment"
msgid "Configure the activities system"
msgstr "Sett opp aktivitetssystemet"

#. +> trunk5
#: tests/imports/org.kde.listactivitiestest/metadata.desktop:3
msgctxt "Name"
msgid "Activities testing"
msgstr "Aktiviteter"

#. +> trunk5
#: tests/imports/plasma-applet-org.kde.listactivitiestest.desktop:2
msgctxt "Name"
msgid "List activities test"
msgstr "List aktivitetsstester"

#. +> trunk5
#: tests/imports/plasma-applet-org.kde.listactivitiestest.desktop:44
msgctxt "Comment"
msgid "Strange, but not a Clock"
msgstr "Underlig, men ingen klokke"

#. +> stable
#: src/service/plugins/activityranking/activitymanager-plugin-activityranking.desktop:2
msgctxt "Name"
msgid "Activity ranking"
msgstr "Aktivitetsrangering"

#. +> stable
#: src/service/plugins/activityranking/activitymanager-plugin-activityranking.desktop:50
msgctxt "Comment"
msgid "Plugin to rank activities based on usage"
msgstr "Programtillegg som rangerer aktiviteter etter bruk"

#. +> stable
#: src/service/plugins/nepomuk/activitymanager-plugin-nepomuk.desktop:2
msgctxt "Name"
msgid "Nepomuk Feeder"
msgstr "Nepomuk innmating"

#. +> stable
#: src/service/plugins/nepomuk/activitymanager-plugin-nepomuk.desktop:48
msgctxt "Comment"
msgid "Plugin to synchronize data with Nepomuk"
msgstr "Programtillegg for å synkronisere data med Nepomuk"

#~ msgctxt "Name"
#~ msgid "Activity manager UI handler"
#~ msgstr "Brukerflatehåndtering for aktivitetsbehandler"

#~ msgctxt "Name"
#~ msgid "Activity ranking plugin"
#~ msgstr "Programtillegg for aktivitetsrangering"

#~ msgctxt "Name"
#~ msgid "Dummy plugin"
#~ msgstr "Attrapp-programtillegg"

#~ msgctxt "Comment"
#~ msgid "Just testing"
#~ msgstr "Bare tester"

#~ msgctxt "Name"
#~ msgid "Global Shortcuts plugin"
#~ msgstr "Programtillegg for globale snarveier"

#~ msgctxt "Comment"
#~ msgid "Adds global keyboard shortcuts to KActivityManager Daemon"
#~ msgstr "Legger til globale snarveistaster til KActivityManager-nissesn"

#~ msgctxt "Name"
#~ msgid "Share-Like-Connect Plugin"
#~ msgstr "Share-Like-Connect programtillegg"

#~ msgctxt "Comment"
#~ msgid "Plugin to provide data to the SLC system"
#~ msgstr "Programtillegg som leverer data til SLC-systemet"

#~ msgctxt "Name"
#~ msgid "Sqlite Feeder Plugin"
#~ msgstr "Programtillegg for Sqlite innmating"

# Translation of kreport_mapsplugin_qt to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-05-29 10:03+0200\n"
"PO-Revision-Date: 2011-10-14 18:52+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: qtrich\n"

#. +> trunk5
#: KoReportItemMaps.cpp:65
#, fuzzy
#| msgid "Data Source"
msgctxt "KoReportItemMaps|"
msgid "Data Source"
msgstr "Datakilde"

#. +> trunk5
#: KoReportItemMaps.cpp:67
#, fuzzy
#| msgid "Latitude:"
msgctxt "KoReportItemMaps|"
msgid "Latitude"
msgstr "Bredde:"

#. +> trunk5
#: KoReportItemMaps.cpp:73
#, fuzzy
#| msgid "Longitude:"
msgctxt "KoReportItemMaps|"
msgid "Longitude"
msgstr "Lengde:"

#. +> trunk5
#: KoReportItemMaps.cpp:79
#, fuzzy
#| msgctxt "@action"
#| msgid "Zoom"
msgctxt "KoReportItemMaps|"
msgid "Zoom"
msgstr "Forstørr/minsk"

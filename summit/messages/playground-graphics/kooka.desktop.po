# Translation of kooka.desktop to Norwegian Bokmål
#
msgid ""
msgstr ""
"Project-Id-Version: desktop files\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-07-28 09:37+0200\n"
"PO-Revision-Date: 2007-09-11 22:44+0200\n"
"Last-Translator: MagicPO 0.3 (automated)\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MagicPO 0.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk
#: kooka/desktop/kooka.desktop:7
#, fuzzy
#| msgid "&Program"
msgctxt "GenericName"
msgid "Scan & OCR Program"
msgstr "&Program"

#. +> trunk
#: kooka/desktop/kooka.desktop:41
#, fuzzy
#| msgid "Kooka"
msgctxt "Name"
msgid "Kooka"
msgstr "Kooka"

#. +> trunk
#: libkscan/desktop/scanservice.desktop:4
#, fuzzy
#| msgctxt "Stencils"
#| msgid "SDL - Service"
msgctxt "Name"
msgid "KDE Scan Service"
msgstr "SDL – Tjeneste"

#~ msgctxt "Name"
#~ msgid "General"
#~ msgstr "Generelt"

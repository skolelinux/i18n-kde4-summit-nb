# Translation of kdontchangethehostname to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2011-08-02 08:57+0200\n"
"PO-Revision-Date: 2010-07-25 16:59+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk stable
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Steensrud"

#. +> trunk stable
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjornst@skogkatt.homelinux.org"

#. +> trunk stable
#: khostname.cpp:72
msgid "Error: HOME environment variable not set.\n"
msgstr "Feil: miljøvariabelen HOME er ikke oppgitt.\n"

#. +> trunk stable
#: khostname.cpp:82
msgid "Error: DISPLAY environment variable not set.\n"
msgstr "Feil: miljøvariabelen DISPLAY er ikke oppgitt.\n"

#. +> trunk stable
#: khostname.cpp:217
msgid "KDontChangeTheHostName"
msgstr "KDontChangeTheHostName"

#. +> trunk stable
#: khostname.cpp:218
msgid "Informs KDE about a change in hostname"
msgstr "Gir KDE melding om en endring i vertsnavnet"

#. +> trunk stable
#: khostname.cpp:219
msgid "(c) 2001 Waldo Bastian"
msgstr "© 2001 Waldo Bastian"

#. +> trunk stable
#: khostname.cpp:220
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#. +> trunk stable
#: khostname.cpp:220
msgid "Author"
msgstr "Utvikler"

#. +> trunk stable
#: khostname.cpp:223
msgid "Old hostname"
msgstr "Tidligere vertsnavn"

#. +> trunk stable
#: khostname.cpp:224
msgid "New hostname"
msgstr "Nytt vertsnavn"

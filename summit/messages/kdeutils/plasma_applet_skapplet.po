# Translation of plasma_applet_skapplet to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_skapplet\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2011-08-02 09:01+0200\n"
"PO-Revision-Date: 2008-09-14 16:34+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk stable
#: skapplet.cpp:141
msgid ""
"*.skz *.theme|Theme Files\n"
"*|All Files"
msgstr ""
"*.skz *.theme|Temafiler\n"
"*|Alle filer"

#. +> trunk stable
#: skapplet.cpp:145 skpackage.cpp:69 skpackage.cpp:95
msgid "SuperKaramba Theme"
msgstr "SuperKaramba tema"

#. +> trunk stable
#: skappletscript.cpp:236
msgid "Failed to launch SuperKaramba Theme"
msgstr "Klarte ikke å starte SuperKaramba-tema"

# Translation of kdevgit to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-03-30 10:33+0200\n"
"PO-Revision-Date: 2011-10-23 00:10+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 1.2\n"

#. +> trunk5 stable
#: gitmessagehighlighter.cpp:79
#, kde-format
msgid "Try to keep summary length below %1 characters."
msgstr "Prøv å holde lengden på sammendrag under %1 tegn."

#. +> trunk5 stable
#: gitmessagehighlighter.cpp:95
msgid "Separate summary from details with one empty line."
msgstr "Skill sammendrag fra detaljer med én tom linje."

#. +> trunk5 stable
#: gitmessagehighlighter.cpp:105
#, kde-format
msgid "Try to keep line length below %1 characters."
msgstr "Prøv å holde linjene kortere enn %1 tegn."

#. +> stable
#: gitplugin.cpp:64
msgid "Git"
msgstr "Git"

#. +> stable
#: gitplugin.cpp:64
msgid "A plugin to support git version control systems"
msgstr "Et programtillegg som gir støtte for versjonskontrollsystemet gittttt"

#. +> trunk5 stable
#: gitplugin.cpp:183
msgid "git is not installed"
msgstr "git er ikke installert"

#. +> trunk5 stable
#: gitplugin.cpp:237
msgid "Git Stashes"
msgstr "Git-stabler"

#. +> trunk5 stable
#: gitplugin.cpp:238 stashmanagerdialog.cpp:42
msgid "Stash Manager"
msgstr "Stabelbehandler"

#. +> trunk5 stable
#: gitplugin.cpp:239
msgid "Push Stash"
msgstr "Push stabel"

#. +> trunk5 stable
#: gitplugin.cpp:240
msgid "Pop Stash"
msgstr "Pop stabel"

#. +> trunk5 stable
#: gitplugin.cpp:266
#, kde-format
msgid "error: %1"
msgstr "feil: %1"

#. +> trunk5 stable
#: gitplugin.cpp:322 gitplugin.cpp:333
msgid "Did not specify the list of files"
msgstr "Oppga ikke liste over filer"

#. +> trunk5 stable
#: gitplugin.cpp:389
msgid "Could not revert changes"
msgstr "Klarte ikke tilbakestille endringer"

#. +> trunk5 stable
#: gitplugin.cpp:399
msgid "The following files have uncommited changes, which will be lost. Continue?"
msgstr ""

#. +> trunk5 stable
#: gitplugin.cpp:422
msgid "No files or message specified"
msgstr "Ingen filer eller meldinger oppgitt"

#. +> trunk5 stable
#: gitplugin.cpp:473
msgid "No files to remove"
msgstr "Ingen filer som skal fjernes"

#. +> trunk5 stable
#: gitplugin.cpp:652
msgid "There are pending changes, do you want to stash them first?"
msgstr "Det er ventende endringer, vil du stable dem først?"

#. +> trunk5 stable
#: stashmanagerdialog.cpp:114
#, kde-format
msgid "Are you sure you want to drop the stash '%1'?"
msgstr "Er du sikker på at du vil slette stabelen «%1»?"

#. +> trunk5 stable
#: stashmanagerdialog.cpp:122
msgid "KDevelop - Git Stash"
msgstr "KDevelop – Git stabel"

#. +> trunk5 stable
#: stashmanagerdialog.cpp:122
#, fuzzy
#| msgid "Select a name for the new branch"
msgid "Select a name for the new branch:"
msgstr "Velg et navn på den nye grenen"

#. i18n: ectx: property (whatsThis), widget (QPushButton, apply)
#. +> trunk5 stable
#: stashmanagerdialog.ui:17
msgid "Applies stash's patch"
msgstr "Tar stabelens lapp i bruk"

#. i18n: ectx: property (text), widget (QPushButton, apply)
#. +> trunk5 stable
#: stashmanagerdialog.ui:20
msgid "Apply"
msgstr "Bruk"

#. i18n: ectx: property (whatsThis), widget (QPushButton, pop)
#. +> trunk5 stable
#: stashmanagerdialog.ui:27
msgid "Applies stash's patch and drops the stash"
msgstr "Tar stabelens lapp i bruk og sletter stabelen"

#. i18n: ectx: property (text), widget (QPushButton, pop)
#. +> trunk5 stable
#: stashmanagerdialog.ui:30
msgid "Pop"
msgstr "Pop"

#. i18n: ectx: property (whatsThis), widget (QPushButton, branch)
#. +> trunk5 stable
#: stashmanagerdialog.ui:37
msgid "Creates a new branch and applies the stash there, then it drops the stash."
msgstr "Lager en ny gren og tar stabelen i bruk der, og sletter så stabelen."

#. i18n: ectx: property (text), widget (QPushButton, branch)
#. +> trunk5 stable
#: stashmanagerdialog.ui:40
msgid "Branch"
msgstr "Gren"

#. i18n: ectx: property (whatsThis), widget (QPushButton, drop)
#. +> trunk5 stable
#: stashmanagerdialog.ui:60
msgid "Removes the selected branch"
msgstr "Fjerner den valgte grenen"

#. i18n: ectx: property (text), widget (QPushButton, drop)
#. +> trunk5 stable
#: stashmanagerdialog.ui:63
msgid "Drop"
msgstr "Ta bort"

#. i18n: ectx: property (whatsThis), widget (QPushButton, show)
#. +> trunk5 stable
#: stashmanagerdialog.ui:70
#, fuzzy
#| msgid "Empty the contents of the trash"
msgid "Show the contents of the stash"
msgstr "Tøm innholdet i papirkurven"

#. i18n: ectx: property (text), widget (QPushButton, show)
#. +> trunk5 stable
#: stashmanagerdialog.ui:73
msgid "Show"
msgstr "Vis"

#. +> stable
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Steensrud"

#. +> stable
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjornst@skogkatt.homelinux.org"

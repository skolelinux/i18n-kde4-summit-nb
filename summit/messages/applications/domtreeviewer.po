# Translation of domtreeviewer to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2002, 2005, 2008, 2009, 2011.
# Knut Yrvin <knut.yrvin@gmail.com>, 2003.
# Axel Bojer <axel@bojer.no>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: domtreeviewer\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2012-01-04 09:19+0100\n"
"PO-Revision-Date: 2011-05-20 21:08+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#. +> trunk stable
#: attributeeditwidget.ui:24
msgid "Attribute &name:"
msgstr "Egenskaps&navn:"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#. +> trunk stable
#: attributeeditwidget.ui:42
msgid "Attribute &value:"
msgstr "Egenskaps&verdi:"

#. +> trunk stable
#: domtreecommands.cpp:33
msgid "No error"
msgstr "Ingen feil"

#. +> trunk stable
#: domtreecommands.cpp:34
msgid "Index size exceeded"
msgstr "Indeksstørrelsen er overskredet"

#. +> trunk stable
#: domtreecommands.cpp:35
msgid "DOMString size exceeded"
msgstr "Størrelsen på DOM-strengen er overskredet"

#. +> trunk stable
#: domtreecommands.cpp:36
msgid "Hierarchy request error"
msgstr "Feil i hierarki-forespørsel"

#. +> trunk stable
#: domtreecommands.cpp:37
msgid "Wrong document"
msgstr "Feil dokument"

#. +> trunk stable
#: domtreecommands.cpp:38
msgid "Invalid character"
msgstr "Ugyldig tegn"

#. +> trunk stable
#: domtreecommands.cpp:39
msgid "No data allowed"
msgstr "Ingen data tillatt"

#. +> trunk stable
#: domtreecommands.cpp:40
msgid "No modification allowed"
msgstr "Ingen endring tillatt"

#. +> trunk stable
#: domtreecommands.cpp:41
msgid "Not found"
msgstr "Ikke funnet"

#. +> trunk stable
#: domtreecommands.cpp:42
msgid "Not supported"
msgstr "Ikke støttet"

#. +> trunk stable
#: domtreecommands.cpp:43
msgid "Attribute in use"
msgstr "Egenskapen er i bruk"

#. +> trunk stable
#: domtreecommands.cpp:44
msgid "Invalid state"
msgstr "Ugyldig tilstand"

#. +> trunk stable
#: domtreecommands.cpp:45
msgid "Syntax error"
msgstr "Syntaksfeil"

#. +> trunk stable
#: domtreecommands.cpp:46
msgid "Invalid modification"
msgstr "Ugyldig endring"

#. +> trunk stable
#: domtreecommands.cpp:47
msgid "Namespace error"
msgstr "Navnerom-feil"

#. +> trunk stable
#: domtreecommands.cpp:48
msgid "Invalid access"
msgstr "Ugyldig tilgang"

#. +> trunk stable
#: domtreecommands.cpp:56
#, kde-format
msgid "Unknown Exception %1"
msgstr "Ukjent unntak %1"

#. +> trunk stable
#: domtreecommands.cpp:300
msgid "Add attribute"
msgstr "Legg til egenskap"

#. +> trunk stable
#: domtreecommands.cpp:330
msgid "Change attribute value"
msgstr "Endre egenskapsverdi"

#. +> trunk stable
#: domtreecommands.cpp:361
msgid "Remove attribute"
msgstr "Fjern egenskap"

#. +> trunk stable
#: domtreecommands.cpp:393
msgid "Rename attribute"
msgstr "Endre navn på egenskap"

#. +> trunk stable
#: domtreecommands.cpp:429
msgid "Change textual content"
msgstr "Endre tekstinnhold"

#. +> trunk stable
#: domtreecommands.cpp:491
msgid "Insert node"
msgstr "Sett inn node"

#. +> trunk stable
#: domtreecommands.cpp:519
msgid "Remove node"
msgstr "Fjern node"

#. +> trunk stable
#: domtreecommands.cpp:565
msgid "Move node"
msgstr "Flytt node"

#. +> trunk stable
#: domtreeview.cpp:70
msgctxt "@title:window"
msgid "Edit Element"
msgstr "Rediger element"

#. +> trunk stable
#: domtreeview.cpp:72 domtreeview.cpp:92
msgid "&Append as Child"
msgstr "L&egg til som barn"

#. +> trunk stable
#: domtreeview.cpp:73 domtreeview.cpp:93
msgid "Insert &Before Current"
msgstr "Sett inn &før gjeldende"

#. +> trunk stable
#: domtreeview.cpp:90
msgctxt "@title:window"
msgid "Edit Text"
msgstr "Rediger tekst"

#. +> trunk stable
#: domtreeview.cpp:110
msgctxt "@title:window"
msgid "Edit Attribute"
msgstr "Rediger egenskap"

#. +> trunk stable
#: domtreeview.cpp:172
#, kde-format
msgctxt "@title:window"
msgid "DOM Tree for %1"
msgstr "DOM-tre for %1"

#. +> trunk stable
#: domtreeview.cpp:172
msgctxt "@title:window"
msgid "DOM Tree"
msgstr "DOM-tre"

#. +> trunk stable
#: domtreeview.cpp:514
msgid "Move Nodes"
msgstr "Flytt noder"

#. +> trunk stable
#: domtreeview.cpp:579
msgid "Save DOM Tree as HTML"
msgstr "Lagre DOM-tre som HTML"

#. +> trunk stable
#: domtreeview.cpp:584
msgctxt "@title:window"
msgid "File Exists"
msgstr "Fila finnes"

#. +> trunk stable
#: domtreeview.cpp:585
#, kde-format
msgid ""
"Do you really want to overwrite: \n"
"%1?"
msgstr ""
"Er du sikker på at du vil skrive over: \n"
"%1?"

#. +> trunk stable
#: domtreeview.cpp:586
msgid "Overwrite"
msgstr "Skriv over"

#. +> trunk stable
#: domtreeview.cpp:599
msgctxt "@title:window"
msgid "Unable to Open File"
msgstr "Kan ikke åpne fil"

#. +> trunk stable
#: domtreeview.cpp:600
#, kde-format
msgid ""
"Unable to open \n"
" %1 \n"
" for writing"
msgstr ""
"Kan ikke åpne \n"
" %1 \n"
" for skriving"

#. +> trunk stable
#: domtreeview.cpp:604
msgctxt "@title:window"
msgid "Invalid URL"
msgstr "Ugyldig URL"

#. +> trunk stable
#: domtreeview.cpp:605
#, kde-format
msgid ""
"This URL \n"
" %1 \n"
" is not valid."
msgstr ""
"Denne URL-en \n"
" %1 \n"
" er ikke gyldig."

#. +> trunk stable
#: domtreeview.cpp:823
msgid "Delete Nodes"
msgstr "Slett noder"

#. +> trunk stable
#: domtreeview.cpp:1034
msgid "<Click to add>"
msgstr "&lt;Trykk for å legge til&gt;"

#. +> trunk stable
#: domtreeview.cpp:1366
msgid "Delete Attributes"
msgstr "Slett egenskaper"

#. i18n: ectx: property (windowTitle), widget (QWidget, DOMTreeViewBase)
#. +> trunk stable
#: domtreeviewbase.ui:19
msgid "DOM Tree Viewer"
msgstr "DOM-tre-viser"

#. i18n: ectx: property (text), widget (KPushButton, messageListBtn)
#. +> trunk stable
#: domtreeviewbase.ui:61
msgid "&List"
msgstr "&Liste"

#. i18n: ectx: property (text), widget (KPushButton, messageHideBtn)
#. +> trunk stable
#: domtreeviewbase.ui:92
msgid "H&ide"
msgstr "S&kjul"

#. i18n: ectx: property (text), widget (QTreeWidget, m_listView)
#. +> trunk stable
#: domtreeviewbase.ui:115
msgid "DOM Tree"
msgstr "DOM-tre"

#. i18n: ectx: attribute (title), widget (QWidget, domTab)
#. +> trunk stable
#: domtreeviewbase.ui:127
msgid "DOM Node"
msgstr "DOM-node"

#. i18n: ectx: property (text), widget (QLabel, textLabel1_2_3)
#. +> trunk stable
#: domtreeviewbase.ui:138
msgid "Node &value:"
msgstr "Node&verdi:"

#. i18n: ectx: property (text), widget (QLabel, textLabel1_2_2)
#. +> trunk stable
#: domtreeviewbase.ui:151
msgid "Node &type:"
msgstr "Node&type:"

#. i18n: ectx: property (text), widget (QLabel, textLabel1_2)
#. +> trunk stable
#: domtreeviewbase.ui:164
msgid "Namespace &URI:"
msgstr "&URL for navnerom:"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#. +> trunk stable
#: domtreeviewbase.ui:177
msgid "Node &name:"
msgstr "Node&navn:"

#. i18n: ectx: property (text), widget (QTreeWidget, nodeAttributes)
#. +> trunk stable
#: domtreeviewbase.ui:249
msgid "Name"
msgstr "Navn"

#. i18n: ectx: property (text), widget (QTreeWidget, nodeAttributes)
#. i18n: ectx: property (text), widget (QTreeWidget, cssProperties)
#. +> trunk stable
#: domtreeviewbase.ui:254 domtreeviewbase.ui:344
msgid "Value"
msgstr "Verdi"

#. i18n: ectx: property (text), widget (QPushButton, applyContent)
#. +> trunk stable
#: domtreeviewbase.ui:287
msgid "Appl&y"
msgstr "Br&uk"

#. i18n: ectx: attribute (title), widget (QWidget, cssTab)
#. +> trunk stable
#: domtreeviewbase.ui:320
msgid "Computed Style"
msgstr "Beregnet stil"

#. i18n: ectx: property (text), widget (QTreeWidget, cssProperties)
#. +> trunk stable
#: domtreeviewbase.ui:339
msgid "Property"
msgstr "Egenskap"

#. i18n: ectx: attribute (title), widget (QWidget, styleSheetsTab)
#. +> trunk stable
#: domtreeviewbase.ui:353
msgid "Stylesheets"
msgstr "Stilark"

#. i18n: ectx: property (text), widget (QTreeWidget, styleSheetsTree)
#. +> trunk stable
#: domtreeviewbase.ui:366
msgid "1"
msgstr "1"

#. i18n: ectx: Menu (file)
#. +> trunk stable
#: domtreeviewerui.rc:4
msgid "&File"
msgstr "&Fil"

#. i18n: ectx: Menu (edit)
#. +> trunk stable
#: domtreeviewerui.rc:6
msgid "&Edit"
msgstr "&Rediger"

#. i18n: ectx: Menu (view)
#. +> trunk stable
#: domtreeviewerui.rc:8
msgid "&View"
msgstr "&Vis"

#. i18n: ectx: Menu (go)
#. +> trunk stable
#: domtreeviewerui.rc:19
msgid "&Go"
msgstr "&Gå"

#. i18n: ectx: ToolBar (mainToolBar)
#. +> trunk stable
#: domtreeviewerui.rc:23
msgid "Main Toolbar"
msgstr "Hovedverktøylinje"

#. i18n: ectx: ToolBar (treeToolBar)
#. +> trunk stable
#: domtreeviewerui.rc:25
msgid "Tree Toolbar"
msgstr "Tre-verktøylinje"

#. +> trunk stable
#: domtreewindow.cpp:62
msgctxt "@title:window"
msgid "Message Log"
msgstr "Meldingslogg"

#. +> trunk stable
#: domtreewindow.cpp:169
msgid "Pure DOM Tree"
msgstr "Rent DOM-tre"

#. +> trunk stable
#: domtreewindow.cpp:175
msgid "Show DOM Attributes"
msgstr "Vis DOM-egenskaper"

#. +> trunk stable
#: domtreewindow.cpp:182
msgid "Highlight HTML"
msgstr "Framhev HTML"

#. +> trunk stable
#: domtreewindow.cpp:190
msgid "Show Message Log"
msgstr "Vis meldingslogg"

#. +> trunk stable
#: domtreewindow.cpp:202
msgid "Expand"
msgstr "Utvid"

#. +> trunk stable
#: domtreewindow.cpp:205
msgid "Increase expansion level"
msgstr "Øk utvidelsesnivået"

#. +> trunk stable
#: domtreewindow.cpp:208
msgid "Collapse"
msgstr "Fold sammen"

#. +> trunk stable
#: domtreewindow.cpp:211
msgid "Decrease expansion level"
msgstr "Senk utvidelsesnivået"

#. +> trunk stable
#: domtreewindow.cpp:217 domtreewindow.cpp:235
msgid "&Delete"
msgstr "&Slett"

#. +> trunk stable
#: domtreewindow.cpp:220
msgid "Delete nodes"
msgstr "Slett noder"

#. +> trunk stable
#: domtreewindow.cpp:225
msgid "New &Element..."
msgstr "Nytt &element …"

#. +> trunk stable
#: domtreewindow.cpp:229
msgid "New &Text Node..."
msgstr "Ny &tekstnode …"

#. +> trunk stable
#: domtreewindow.cpp:238
msgid "Delete attributes"
msgstr "Slett egenskaper"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#. +> trunk stable
#: elementeditwidget.ui:24
msgid "Element &name:"
msgstr "Element&navn:"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#. +> trunk stable
#: elementeditwidget.ui:47
msgid "Element &namespace:"
msgstr "Na&vnerom for element:"

#. +> trunk stable
#: plugin_domtreeviewer.cpp:24
msgid "Show &DOM Tree"
msgstr "Vis &DOM-tre"

#. i18n: ectx: Menu (tools)
#. +> trunk stable
#: plugin_domtreeviewer.rc:4
msgid "&Tools"
msgstr "&Verktøy"

#. i18n: ectx: ToolBar (extraToolBar)
#. +> trunk stable
#: plugin_domtreeviewer.rc:8
msgid "Extra Toolbar"
msgstr "Ekstra verktøylinje"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#. +> trunk stable
#: texteditwidget.ui:19
msgid "Edit &text for text node:"
msgstr "Rediger &tekst for tekstnode:"

#~ msgid "Edit Element"
#~ msgstr "Rediger element"

#~ msgid "Edit Text"
#~ msgstr "Rediger tekst"

#~ msgid "Edit Attribute"
#~ msgstr "Rediger egenskap"

#~ msgid "DOM Tree for %1"
#~ msgstr "DOM-tre for %1"

#~ msgid "File Exists"
#~ msgstr "Fila finnes"

#~ msgid "Unable to Open File"
#~ msgstr "Kan ikke åpne fil"

#~ msgid "Invalid URL"
#~ msgstr "Ugyldig URL"

#~ msgid "Message Log"
#~ msgstr "Meldingslogg"

# Translation of kdialog to Norwegian Bokmål
#
# Knut Erik Hollund <khollund@responze.net>, 2003.
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 2004.
# Knut Yrvin <knut.yrvin@gmail.com>, 2005.
# Eskild Hustvedt <zerodogg@skolelinux.no>, 2005.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2008, 2009, 2010, 2011, 2013.
msgid ""
msgstr ""
"Project-Id-Version: kdialog\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2013-11-06 09:02+0100\n"
"PO-Revision-Date: 2013-11-26 12:44+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk stable
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Knut Yrvin,Knut Erik Hollund,Eskild Hustvedt"

#. +> trunk stable
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "knut.yrvin@gmail.com,khollund@responze.net,zerodogg@skolelinux.no"

#. +> trunk stable
#: kdialog.cpp:572 kdialog.cpp:688
msgctxt "@title:window"
msgid "Open"
msgstr "Åpne"

#. +> trunk stable
#: kdialog.cpp:617
msgctxt "@title:window"
msgid "Save As"
msgstr "Lagre som"

#. +> trunk stable
#: kdialog.cpp:792
msgctxt "@title:window"
msgid "Choose Color"
msgstr "Velg farge"

#. +> trunk stable
#: kdialog.cpp:843
msgid "KDialog"
msgstr "KDialog"

#. +> trunk stable
#: kdialog.cpp:844
msgid "KDialog can be used to show nice dialog boxes from shell scripts"
msgstr "KDialog kan brukes for å vise hyggelige dialogvinduer fra skallskript"

#. +> trunk stable
#: kdialog.cpp:846
msgid "(C) 2000, Nick Thompson"
msgstr "© 2000 Nick Thompson"

#. +> trunk stable
#: kdialog.cpp:847
msgid "David Faure"
msgstr "David Faure"

#. +> trunk stable
#: kdialog.cpp:847
msgid "Current maintainer"
msgstr "Gjeldende vedlikeholder"

#. +> trunk stable
#: kdialog.cpp:848
msgid "Brad Hards"
msgstr "Brad Hards"

#. +> trunk stable
#: kdialog.cpp:849
msgid "Nick Thompson"
msgstr "Nick Thompson"

#. +> trunk stable
#: kdialog.cpp:850
msgid "Matthias Hölzer"
msgstr "Matthias Hölzer"

#. +> trunk stable
#: kdialog.cpp:851
msgid "David Gümbel"
msgstr "David Gümbel"

#. +> trunk stable
#: kdialog.cpp:852
msgid "Richard Moore"
msgstr "Richard Moore"

#. +> trunk stable
#: kdialog.cpp:853
msgid "Dawit Alemayehu"
msgstr "Dawit Alemayehu"

#. +> trunk stable
#: kdialog.cpp:854
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#. +> trunk stable
#: kdialog.cpp:860
msgid "Question message box with yes/no buttons"
msgstr "Spørreboks med ja/nei-knapper"

#. +> trunk stable
#: kdialog.cpp:861
msgid "Question message box with yes/no/cancel buttons"
msgstr "Spørreboks med ja/nei/avbryt-knapper"

#. +> trunk stable
#: kdialog.cpp:862
msgid "Warning message box with yes/no buttons"
msgstr "Avarselboks med ja/nei-knapper"

#. +> trunk stable
#: kdialog.cpp:863
msgid "Warning message box with continue/cancel buttons"
msgstr "Advarselboks med fortsett/avbryt-knapper"

#. +> trunk stable
#: kdialog.cpp:864
msgid "Warning message box with yes/no/cancel buttons"
msgstr "Advarselboks med ja/nei/avbryt-knapper"

#. +> trunk stable
#: kdialog.cpp:865
msgid "Use text as Yes button label"
msgstr "Bruk tekst som etikett på Ja-knappen"

#. +> trunk stable
#: kdialog.cpp:866
msgid "Use text as No button label"
msgstr "Bruk tekst som etikett på Nei-knappen"

#. +> trunk stable
#: kdialog.cpp:867
msgid "Use text as Cancel button label"
msgstr "Bruk tekst som etikett på Avbryt-knappen"

#. +> trunk stable
#: kdialog.cpp:868
msgid "Use text as Continue button label"
msgstr "Bruk tekst som etikett på Fortsett-knappen"

#. +> trunk stable
#: kdialog.cpp:869
msgid "'Sorry' message box"
msgstr "'Beklager'-boks"

#. +> trunk stable
#: kdialog.cpp:870
msgid "'Sorry' message box with expandable Details field"
msgstr "«Beklager»-meldingsboks med Detaljer-felt som kan utvides"

#. +> trunk stable
#: kdialog.cpp:871
msgid "'Error' message box"
msgstr "«Feil»-meldingsboks"

#. +> trunk stable
#: kdialog.cpp:872
msgid "'Error' message box with expandable Details field"
msgstr "«Feil»-meldingsboks med Detaljer-felt som kan utvides"

#. +> trunk stable
#: kdialog.cpp:873
msgid "Message Box dialog"
msgstr "Vindu med meldingsboks"

#. +> trunk stable
#: kdialog.cpp:874
msgid "Input Box dialog"
msgstr "Vindu med inndataboks"

#. +> trunk stable
#: kdialog.cpp:875
msgid "Password dialog"
msgstr "Passordvindu"

#. +> trunk stable
#: kdialog.cpp:876
msgid "Text Box dialog"
msgstr "Tekstboksvindu"

#. +> trunk stable
#: kdialog.cpp:877
msgid "Text Input Box dialog"
msgstr "Dialog med inndataboks"

#. +> trunk stable
#: kdialog.cpp:878
msgid "ComboBox dialog"
msgstr "Vindu med kombinasjonsboks"

#. +> trunk stable
#: kdialog.cpp:879
msgid "Menu dialog"
msgstr "Menyvindu"

#. +> trunk stable
#: kdialog.cpp:880
msgid "Check List dialog"
msgstr "Sjekklistevindu"

#. +> trunk stable
#: kdialog.cpp:881
msgid "Radio List dialog"
msgstr "Radioknapp-vindu"

#. +> trunk stable
#: kdialog.cpp:882
msgid "Passive Popup"
msgstr "Passiv sprettopp"

#. +> trunk stable
#: kdialog.cpp:883
msgid "File dialog to open an existing file"
msgstr "Fildialog for å åpne en fil"

#. +> trunk stable
#: kdialog.cpp:884
msgid "File dialog to save a file"
msgstr "Fildialog for å lagre en fil"

#. +> trunk stable
#: kdialog.cpp:885
msgid "File dialog to select an existing directory"
msgstr "Fildialog for å velge en eksisterende mappe"

#. +> trunk stable
#: kdialog.cpp:886
msgid "File dialog to open an existing URL"
msgstr "Fildialog for å åpne en URL"

#. +> trunk stable
#: kdialog.cpp:887
msgid "File dialog to save a URL"
msgstr "Fildialog for å lagre en URL"

#. +> trunk stable
#: kdialog.cpp:888
msgid "Icon chooser dialog"
msgstr "Dialog for valg av ikon"

#. +> trunk stable
#: kdialog.cpp:889
msgid "Progress bar dialog, returns a D-Bus reference for communication"
msgstr "Dialog for framdriftsviser, returnerer en D-bus-referanse for kommunikasjon"

#. +> trunk stable
#: kdialog.cpp:890
msgid "Color dialog to select a color"
msgstr "Fargedialog for å velge en farge"

#. +> trunk stable
#: kdialog.cpp:892
msgid "Dialog title"
msgstr "Dialogoverskrift"

#. +> trunk stable
#: kdialog.cpp:893
msgid "Default entry to use for combobox, menu and color"
msgstr "Standard linje for bruk av kombinasjonsboks, meny og farge"

#. +> trunk stable
#: kdialog.cpp:894
msgid "Allows the --getopenurl and --getopenfilename options to return multiple files"
msgstr "Tillat --getopenurl og --getopenfilename  å hente flere filer"

#. +> trunk stable
#: kdialog.cpp:895
msgid "Return list items on separate lines (for checklist option and file open with --multiple)"
msgstr "Returner listeelementer i separate linjer (for sjekklistevalg og filer åpnet med --multiple)"

#. +> trunk stable
#: kdialog.cpp:896
msgid "Outputs the winId of each dialog"
msgstr "Skriver ut winId for hver dialog"

#. +> trunk stable
#: kdialog.cpp:897
msgid "Config file and option name for saving the \"do-not-show/ask-again\" state"
msgstr "Oppsettsfil og navn på alternativ for lagring av status for «ikke vis/spør igjen»"

#. +> trunk stable
#: kdialog.cpp:898
msgid "Slider dialog box, returns selected value"
msgstr "Glidebryter-dialogboks, returnerer valgt verdi"

#. +> trunk stable
#: kdialog.cpp:899
msgid "Calendar dialog box, returns selected date"
msgstr "Kalenderdialogboks, returnerer valgt dato"

#. +> trunk stable
#: kdialog.cpp:902
msgid "Makes the dialog transient for an X app specified by winid"
msgstr "Gjør dialogen forbigående for et X-program oppgitt ved winId"

#. +> trunk stable
#: kdialog.cpp:905
msgid "Arguments - depending on main option"
msgstr "Argumenter – avhengig av hovedvalg"

#. +> trunk stable
#: widgets.cpp:117
#, kde-format
msgid "kdialog: could not open file %1"
msgstr "kdialog: klarte ikke åpne fila %1"

#~ msgid "Open"
#~ msgstr "Åpne"

#~ msgid "Save As"
#~ msgstr "Lagre som"

#~ msgid "Choose Color"
#~ msgstr "Velg farge"

# Translation of kcontrol_history to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-03-20 16:13+0100\n"
"PO-Revision-Date: 2011-09-10 23:01+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: docbook4\n"

#. Tag: title
#. +> trunk5 stable5 trunk stable
#: index.docbook:8
#, no-c-format
msgid "History Sidebar"
msgstr "Historie-sidestolpe"

#. Tag: author
#. +> trunk5 stable5 trunk stable
#: index.docbook:11
#, no-c-format
msgid "&Burkhard.Lueck;"
msgstr "&Burkhard.Lueck;"

#. Tag: trans_comment
#. +> trunk5 stable5 trunk stable
#: index.docbook:12
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr "<othercredit role=\"translator\"><firstname>Bjørn</firstname><surname>Steensrud</surname><affiliation><address><email>bjornst@skogkatt.homelinux.org</email></address></affiliation><contrib>Oversettelse</contrib></othercredit>"

#. Tag: date
#. +> trunk5 stable5 trunk stable
#: index.docbook:15
#, no-c-format
msgid "2009-11-24"
msgstr "2009-11-24"

#. Tag: releaseinfo
#. +> trunk5 stable5 trunk stable
#: index.docbook:16
#, no-c-format
msgid "&kde; 4.4"
msgstr "&kde; 4.4"

#. Tag: keyword
#. +> trunk5 stable5 trunk stable
#: index.docbook:19
#, no-c-format
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#. Tag: keyword
#. +> trunk5 stable5 trunk stable
#: index.docbook:20
#, no-c-format
msgid "Systemsettings"
msgstr "Systeminnstillinger"

#. Tag: keyword
#. +> trunk5 stable5 trunk stable
#: index.docbook:21
#, no-c-format
msgid "history"
msgstr "historie"

#. Tag: para
#. +> trunk5 stable5 trunk stable
#: index.docbook:25
#, no-c-format
msgid "This dialog allows you to configure the history sidebar."
msgstr "Dette dialogvinduet kan brukes til å sette opp historie-sidestolpen."

#. Tag: guilabel
#. +> trunk5 stable5 trunk stable
#: index.docbook:30
#, no-c-format
msgid "Limits"
msgstr "Grenser"

#. Tag: para
#. +> trunk5 stable5 trunk stable
#: index.docbook:31
#, no-c-format
msgid "The options in this section can be used to set the maximum size of your history and set a time after which items are automatically removed."
msgstr "Valgene her kan brukes til å stille inn maksimal størrelse på historien, og oppgi en levetid på innslag i historien slik at de blir automatisk slettet etter en tid."

#. Tag: guilabel
#. +> trunk5 stable5 trunk stable
#: index.docbook:39
#, no-c-format
msgid "Custom Font"
msgstr "Selvvalgt skriftype"

#. Tag: para
#. +> trunk5 stable5 trunk stable
#: index.docbook:40
#, no-c-format
msgid "Here you can also set different fonts for new and old &URL;s by selecting the <guibutton>Choose Font</guibutton> button."
msgstr "Her kan du også oppgi forskjellig skrift for nye og gamle &URL;-er ved å trykke <guibutton>Velg skrift</guibutton>-knappen."

#. Tag: guilabel
#. +> trunk5 stable5 trunk stable
#: index.docbook:48
#, no-c-format
msgid "Details"
msgstr "Detaljer"

#. Tag: para
#. +> trunk5 stable5 trunk stable
#: index.docbook:49
#, no-c-format
msgid "The <guilabel>Detailed tooltips</guilabel> check box controls how much information is displayed when you hover the mouse pointer over an item in the history page."
msgstr "Avkryssingsboksen <guilabel>Detaljerte verktøytips</guilabel> styrer hvor mye informasjon som vises når du legger musepekeren over et element på historiesida."

#. Tag: para
#. +> trunk5 stable5 trunk stable
#: index.docbook:53
#, no-c-format
msgid "If checked the number of times visited and the dates of the first and last visits are shown, in addition to the URL."
msgstr "Hvis det er valgt vises i tillegg til URL-en, hvor mange ganger man har besøkt URL-en og datoene for første og siste gang du gjorde det."

#. Tag: para
#. +> trunk5 stable5 trunk stable
#: index.docbook:59
#, no-c-format
msgid "Selecting <guibutton>Clear History</guibutton> will clear out the entire history."
msgstr "Når du velger <guimenuitem>Tøm historien</guimenuitem> fjerner du hele historien."

# Translation of kcontrol_solid-actions to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-03-20 16:13+0100\n"
"PO-Revision-Date: 2011-10-09 21:46+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: docbook4\n"

#. Tag: title
#. +> trunk5 stable5 stable
#: index.docbook:9
#, no-c-format
msgid "Device Actions"
msgstr "Enhetshandlinger"

#. Tag: author
#. +> trunk5 stable5 stable
#: index.docbook:11
#, no-c-format
msgid "<firstname>Ben</firstname><surname>Cooksley</surname>"
msgstr "<firstname>Ben</firstname><surname>Cooksley</surname>"

#. Tag: author
#. +> trunk5 stable5 stable
#: index.docbook:12
#, no-c-format
msgid "&Anne-Marie.Mahfouf;"
msgstr "&Anne-Marie.Mahfouf;"

#. Tag: trans_comment
#. +> trunk5 stable5 stable
#: index.docbook:13
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr "<othercredit role=\"translator\"><firstname>Bjørn</firstname><surname>Steensrud</surname><affiliation><address><email>bjornst@skogkatt.homelinux.org</email></address></affiliation><contrib>Oversettelse</contrib></othercredit>"

#. Tag: date
#. +> trunk5 stable5 stable
#: index.docbook:16
#, fuzzy, no-c-format
#| msgid "2011-02-05"
msgid "2013-12-05"
msgstr "2011-02-05"

#. Tag: releaseinfo
#. +> trunk5 stable5 stable
#: index.docbook:17
#, fuzzy, no-c-format
#| msgid "&kde; 4.1.2"
msgid "&kde; 4.12"
msgstr "&kde; 4.1.2"

#. Tag: keyword
#. +> trunk5 stable5 stable
#: index.docbook:20
#, no-c-format
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#. Tag: keyword
#. +> trunk5 stable5 stable
#: index.docbook:21
#, no-c-format
msgid "System Settings"
msgstr "Systeminnstillinger"

#. Tag: keyword
#. +> trunk5 stable5 stable
#: index.docbook:22
#, no-c-format
msgid "Solid"
msgstr "Solid"

#. Tag: keyword
#. +> trunk5 stable5 stable
#: index.docbook:23
#, no-c-format
msgid "actions"
msgstr "handlinger"

#. Tag: keyword
#. +> trunk5 stable5 stable
#: index.docbook:24
#, no-c-format
msgid "devices"
msgstr "enheter"

#. Tag: keyword
#. +> trunk5 stable5 stable
#: index.docbook:25
#, no-c-format
msgid "hardware"
msgstr "maskinvare"

#. Tag: title
#. +> trunk5 stable5 stable
#: index.docbook:30
#, no-c-format
msgid "Actions for new devices"
msgstr "Handlinger for nye enheter"

#. Tag: title
#. +> trunk5 stable5 stable
#: index.docbook:32
#, no-c-format
msgid "Introduction"
msgstr "Innledning"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:34
#, no-c-format
msgid "This module allows you to set up the actions you want to associate when you plug new devices."
msgstr "Med denne modulen kan du sette opp handlinger du vil knytte til at nye enheter blir koblet til."

#. Tag: screeninfo
#. +> trunk5 stable5 stable
#: index.docbook:38
#, no-c-format
msgid "<screeninfo>Screenshot of the Device Actions Manager</screeninfo>"
msgstr "<screeninfo>Skjermbilde av Enhetshandling-behandler</screeninfo>"

#. Tag: phrase
#. +> trunk5 stable5 stable
#: index.docbook:44
#, no-c-format
msgid "<phrase>Screenshot of the Device Actions Manager</phrase>"
msgstr "<phrase>Skjermbilde av Enhetshandling-behandler</phrase>"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:50
#, no-c-format
msgid "You will see listed all of the possible actions that are available when devices are plugged in. Please note that they will only be available under certain conditions."
msgstr "Du vil få se en liste over mulige handlinger som blir tilgjengelige når enheter kobles til. Merk at de bare blir tilgjengelige under bestemte forhold."

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:55
#, no-c-format
msgid "<guibutton>Add</guibutton> This will ask you to supply a name for your new action, then will open that action for editing. If you do not supply all the required details in that dialog, then your action will be deleted."
msgstr "<guibutton>Legg til</guibutton> Her blir du bedt m å oppgi et navn på den nye handlingen, og åpner så den nye handlingen for redigering, Hvis du ikke oppgir alle nødvendige detaljer i den dialogen blir handlingen slettet."

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:61
#, no-c-format
msgid "<guibutton>Edit</guibutton> This will allow you to change the Name, Icon, command to execute and the conditions the action must match to be shown."
msgstr "<guibutton>Rediger</guibutton> Her kan du endre navn, ikon, kommando som skal kjøres og betingelsene som en handling må samsvare med for å bli vist."

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:66
#, no-c-format
msgid "<guibutton>Discard / Cannot be deleted / Remove</guibutton> This will do different actions, depending on if it was supplied with your system or created by you. You can only discard changes to system wide actions, if there are no changes then these actions cannot be deleted. User created actions are completely removed, including all changes."
msgstr "<guibutton>Forkast/ Kan ikke slettes/ Fjern</guibutton>  Dette gjør forskjellige ting avhengig av om det ble levert med systemet eller du laget det. Du kan bare forkaste endringer til systemhandlinger, hvis det ikke er noen endringer kan disse handlingene ikke slettes. Brukerdefinerte handlinger blir fullstendig fjernet, med alle endringer."

#. Tag: title
#. +> trunk5 stable5 stable
#: index.docbook:79
#, no-c-format
msgid "Add Action"
msgstr "Legg til handling"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:81
#, no-c-format
msgid "The <guibutton>Add</guibutton> button brings you a dialog to enter the new action name."
msgstr "<guibutton>Legg til</guibutton>-knappen viser en dialog der du kan skrive inn det nye handlingsnavnet."

#. Tag: screeninfo
#. +> trunk5 stable5 stable
#: index.docbook:87
#, no-c-format
msgid "<screeninfo>Screenshot of the Add Action dialog</screeninfo>"
msgstr "<screeninfo>Skjermbilde av dialogen Legg til handlinger</screeninfo>"

#. Tag: phrase
#. +> trunk5 stable5 stable
#: index.docbook:93
#, no-c-format
msgid "<phrase>Screenshot of the Add Action dialog</phrase>"
msgstr "<phrase>Skjermbilde av dialogen Legg til handlinger</phrase>"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:98
#, no-c-format
msgid "The action name should correctly describe the action. After entering a name and clicking <guibutton>OK</guibutton>, the Edit Action dialog will show to allow you to set up the action properties."
msgstr "Navnet på handlingen bør være en riktig beskrivelse av handlingen. Når du har skrevet inn et navn og trykket <guibutton>OK</guibutton> så vil dialogen Rediger Handling bli vist så du kan sette opp handlingens egenskaper."

#. Tag: title
#. +> trunk5 stable5 stable
#: index.docbook:106
#, no-c-format
msgid "Edit Action"
msgstr "Rediger handling"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:108
#, no-c-format
msgid "This dialog allows you to change the settings of the action you selected."
msgstr "I denne dialogen kan du endre innstillinger for den handlingen du valgte."

#. Tag: screeninfo
#. +> trunk5 stable5 stable
#: index.docbook:112
#, no-c-format
msgid "<screeninfo>Screenshot of the Edit Action dialog</screeninfo>"
msgstr "<screeninfo>Skjermbilde av dialogen Rediger handling</screeninfo>"

#. Tag: phrase
#. +> trunk5 stable5 stable
#: index.docbook:118
#, no-c-format
msgid "<phrase>Screenshot of the Edit Action dialog</phrase>"
msgstr "<phrase>Skjermbilde av dialogen Rediger handling</phrase>"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:123
#, no-c-format
msgid "This includes the icon, the action name, the action command and the various parameters. You can change the icon by clicking on it."
msgstr "Dette inkluderer ikonet, handlingsnavnet, handlingskommandoen og dens parametre. Du kan endre ikonet om du trykker på det."

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:127
#, no-c-format
msgid "The parameters are displayed in a tree, the top item having to be true in order for the action to be shown."
msgstr "Parametrene vises i et tre, der det øverste elementet må være sant for at handlingen skal vises."

#. Tag: title
#. +> trunk5 stable5 stable
#: index.docbook:131
#, no-c-format
msgid "Edit parameter"
msgstr "Rediger parameter"

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:134
#, no-c-format
msgid "Parameter type"
msgstr "Parametertype"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:136
#, no-c-format
msgid "This controls what can be contained inside it, or what matching is performed to check if it is true."
msgstr "Dette bestemmer hva som kan inneholdes i parameteren, eller hva som blir gjort for å finne om det er sant."

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:140
#, no-c-format
msgid "Property Match"
msgstr "Egenskapssamsvar"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:142
#, no-c-format
msgid "Compares the specified property of the Device (using <guilabel>Device type</guilabel> and <guilabel>Value name</guilabel>) and using the evaluator, compares it against the text provided."
msgstr "Sammenlikner den oppgitte egenskapen for enheten (med bruk av <guilabel>Enhetstype</guilabel> og <guilabel>Verdinavn</guilabel>) og med evaluatoren sammenlikner med den oppgitte teksten."

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:148
#, no-c-format
msgid "Content Conjunction"
msgstr "Innholdssamstilling"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:150
#, no-c-format
msgid "All sub-properties of this must be true for it to be true itself."
msgstr "Alle under-egenskaper av dette på være sanne for at det skal være sant. (Logisk og)"

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:155
#, no-c-format
msgid "Content Disjunction"
msgstr "Ikke overlappende innhold"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:157
#, no-c-format
msgid "Any sub-property of this may be true for it to be true itself."
msgstr "En eller flere under-egenskaper av dette kan være sant for at dette skal være sant. (Logisk eller)"

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:162
#, no-c-format
msgid "Device interface match"
msgstr "Treff enhetsgrensesnitt"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:164
#, no-c-format
msgid "Checks to see if the Device can be classed as the selected Device type."
msgstr "Ser etter om enheten kan klassifiseres som den valgte enhetstypen."

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:174
#, no-c-format
msgid "Device type"
msgstr "Enhetstype"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:176
#, no-c-format
msgid "These are the possible device types supported by &kde; on your system. They are used in both Property and Device interface matches."
msgstr "Dette er de mulige enhetstypene som &kde; støtter på systemet ditt. De brukes i både Egenskap og Grensenitt-testene."

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:183
#, no-c-format
msgid "Value name"
msgstr "Verdinavn"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:185
#, no-c-format
msgid "This is a list of all possible property names for the selected device type. It also provides the selection of the value evaluator (which is either <guilabel>Equals</guilabel> or <guilabel>Contains</guilabel>), and allows entry of the text to be compared with."
msgstr "Dette er en liste over alle mulige egenskapsnavn for den valgte enhetstypen. Den gir også et valg av verdi-evaluering ( som er enten <guilabel>Lik</guilabel> eller <guilabel>Inneholder</guilabel> og der du kan skrive inn teksten det skal sammenliknes med."

#. Tag: guibutton
#. +> trunk5 stable5 stable
#: index.docbook:193
#, no-c-format
msgid "Reset Parameter"
msgstr "Tilbakestill parameter"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:195
#, no-c-format
msgid "Reverts any unsaved changes you have made to this parameter."
msgstr "Tilbakestiller ulagrede endringer du har gjort til denne parameteren."

#. Tag: guibutton
#. +> trunk5 stable5 stable
#: index.docbook:199
#, no-c-format
msgid "Save Parameter changes"
msgstr "Lagre parameterendringer"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:201
#, no-c-format
msgid "Saves the changes you have made to the parameter."
msgstr "Lagrer endringer du har gjort til denne parameteren."

#~ msgid "2009-11-21"
#~ msgstr "2009-11-21"

#~ msgid "&kde; 4.4"
#~ msgstr "&kde; 4.4"

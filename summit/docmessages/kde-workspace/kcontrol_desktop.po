# Translation of kcontrol_desktop to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2003, 2008, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kcontrol_desktop\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-04-12 10:30+0200\n"
"PO-Revision-Date: 2011-08-08 11:00+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: docbook4\n"
"X-KDE-DocBook-SVN-URL: trunk/KDE/kdebase/runtime/doc/kcontrol/desktop/index.docbook ~~ trunk\n"
"X-KDE-DocBook-SVN-Changed-Revision: 864814 ~~ trunk\n"
"X-KDE-Associated-UI-Catalogs:  ~~ trunk\n"
"X-KDE-DocBook-SVN-URL: branches/KDE/4.3/kdebase/runtime/doc/kcontrol/desktop/index.docbook ~~ stable\n"
"X-KDE-DocBook-SVN-Changed-Revision: 864814 ~~ stable\n"
"X-KDE-Associated-UI-Catalogs:  ~~ stable\n"

#. Tag: title
#. +> trunk5 stable5 stable
#: index.docbook:9 index.docbook:30
#, no-c-format
msgid "Virtual Desktops"
msgstr "Virtuelle skrivebord"

#. Tag: author
#. +> trunk5 stable5 stable
#: index.docbook:13
#, no-c-format
msgid "&Mike.McBride; &Mike.McBride.mail;"
msgstr "&Mike.McBride; &Mike.McBride.mail;"

#. Tag: author
#. +> trunk5 stable5 stable
#: index.docbook:14
#, no-c-format
msgid "&Jost.Schenck; &Jost.Schenck.mail;"
msgstr "&Jost.Schenck; &Jost.Schenck.mail;"

#. Tag: trans_comment
#. +> trunk5 stable5 stable
#: index.docbook:15
#, no-c-format
msgid "ROLES_OF_TRANSLATORS"
msgstr "<othercredit role=\"translator\"><firstname>Bjørn</firstname><surname>Steensrud</surname><affiliation><address><email>bjornst@skogkatt.homelinux.org</email></address></affiliation><contrib></contrib></othercredit>"

#. Tag: date
#. +> trunk5
#: index.docbook:18
#, no-c-format
msgid "2015-04-09"
msgstr ""

#. Tag: date
#. +> stable5 stable
#: index.docbook:18
#, fuzzy, no-c-format
#| msgid "2010-06-23"
msgid "2013-06-02"
msgstr "2010-06-23"

#. Tag: releaseinfo
#. +> trunk5
#: index.docbook:19
#, fuzzy, no-c-format
#| msgid "Plasma"
msgid "Plasma 5.3"
msgstr "Plasma"

#. Tag: releaseinfo
#. +> stable5 stable
#: index.docbook:19
#, no-c-format
msgid "4.11"
msgstr ""

#. Tag: keyword
#. +> trunk5 stable5 stable
#: index.docbook:22
#, no-c-format
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#. Tag: keyword
#. +> trunk5 stable5 stable
#: index.docbook:23
#, no-c-format
msgid "Systemsettings"
msgstr "Systeminnstillinger"

#. Tag: keyword
#. +> trunk5 stable5 stable
#: index.docbook:24
#, no-c-format
msgid "desktop"
msgstr "skrivebord"

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:33
#, no-c-format
msgid "Desktops"
msgstr "Skrivebord"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:35
#, fuzzy, no-c-format
#| msgid "&kde; offers you the possibility to have several virtual desktops. In this tab you can configure the number of desktops as well as their names. Just use the input box to adjust the number of desktops. You can assign names to the desktops by entering text into the text fields below."
msgid "&kde; offers you the possibility to have several virtual desktops. In this tab you can configure the number of desktops, the number of rows in the <guilabel>Pager</guilabel> icon as well as their names. Just use the input box to adjust the number of desktops. You can assign names to the desktops by entering text into the text fields below."
msgstr "&kde; gir deg mulighet til å ha flere virtuelle skrivebord. På dette bladet kan du bestemme antall skrivebord og hva de skal hete. Bare bruk skrivefeltet til å angi antallet skrivebord. Du kan tilordne navn til skrivebordene ved å oppgi tekst i tekstrutene nedenfor."

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:48
#, no-c-format
msgid "Switching"
msgstr "Bytte"

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:52
#, no-c-format
msgid "Desktop navigation wraps around"
msgstr "Skrivebordsnavigering ruller rundt"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:53
#, no-c-format
msgid "Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop."
msgstr "Slå på dette hvis du vil at tastatur- eller aktiv skrivebordsnavigasjon utenfor kanten av et skrivebord skal føre deg til motsatt side av det nye skrivebordet."

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:59
#, no-c-format
msgid "Desktop Effect Animation"
msgstr "Animerte skrivebordseffekter"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:60
#, no-c-format
msgid "Select <guilabel>No Animation</guilabel>, <guilabel>Slide</guilabel>, <guilabel>Desktop Cube Animation</guilabel> or <guilabel>Fade Desktop</guilabel> from the drop down box. If the selected animation has settings options, click on the tools icon on the right of the drop down box to launch a configuration dialog."
msgstr "Velg <guilabel>Ingen animasjon</guilabel>, <guilabel>Skyv</guilabel>, <guilabel>Animert skrivebordsterning</guilabel>eller <guilabel>Ton ut skrivebord</guilabel> fra nedtrekksboksen. Hvis det er innstillinger for den valgte animasjonen, så trykk på verktøy-ikonet til høyre for nedtrekkslista for å åpne et oppsettsvindu."

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:68
#, no-c-format
msgid "Desktop Switch On-Screen Display"
msgstr "Skjermvisning av skrivebordsbytte"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:69
#, no-c-format
msgid "Enable this option if you want to have an on-screen display for desktop switching."
msgstr "Slå på dette hvis du vil ha en skjermvisning for skrivebordsbytte."

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:74
#, no-c-format
msgid "Show desktop layout indicators"
msgstr "Vis indikatorer for skrivebordsutforming"

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:75
#, no-c-format
msgid "Enabling this option will show a small preview of the desktop layout indicating the selected desktop."
msgstr "Om dette er på blir det vist en liten forhåndsvisning av skrivebordsutformingen som viser det valgte skrivebordet."

#. Tag: guilabel
#. +> trunk5 stable5 stable
#: index.docbook:81
#, no-c-format
msgid "Shortcuts"
msgstr "Snarveier"

#. Tag: para
#. +> stable5 stable
#: index.docbook:81
#, no-c-format
msgid "Enable <guilabel>Different widgets for each desktop</guilabel> to use independent widgets and wallpapers on each individual desktop."
msgstr ""

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:82
#, no-c-format
msgid "This section displays the configured shortcuts for switching the desktops and allows you to edit them."
msgstr "Denne delen viser de oppsatte snarveiene for skrivebordsbytte, og her kan de endres."

#. Tag: para
#. +> trunk5 stable5 stable
#: index.docbook:89
#, fuzzy, no-c-format
#| msgid "If you enable <guilabel>Mouse wheel over desktop switches desktop</guilabel> then scrolling the wheel over an empty space on the desktop will change the the next virtual desktop numerically, in the direction you scrolled (either up or down)."
msgid "Scrolling the mouse wheel over an empty space on the desktop or on the <guilabel>Pager</guilabel> icon in the panel will change to the next virtual desktop numerically, in the direction you scrolled (either up or down)."
msgstr "Hvis du slår på <guilabel>Musehjul over skrivebord bytter skrivebord</guilabel> så kan du bytte til neste virtuelle skrivebord ved å rulle musehjulet, i numerisk rekkefølge i den retningen du rullet (opp eller ned)."

#. Tag: para
#. +> trunk5
#: index.docbook:92
#, fuzzy, no-c-format
#| msgid "Changes the view mode to <guimenuitem>Icons</guimenuitem> (<keycombo action=\"simul\">&Ctrl;<keycap>1</keycap></keycombo>), <guimenuitem>Details</guimenuitem> (<keycombo action=\"simul\">&Ctrl;<keycap>2</keycap></keycombo>) or <guimenuitem>Columns</guimenuitem> (<keycombo action=\"simul\">&Ctrl;<keycap>3</keycap></keycombo>)."
msgid "You can change this default behavior on the page <guilabel>Mouse Actions</guilabel> in the <guilabel>Desktop Settings</guilabel> (<keycombo action=\"simul\">&Alt;<keycap>D</keycap></keycombo>, <keycombo action=\"simul\">&Alt;<keycap>S</keycap></keycombo>)."
msgstr "Endrer visningsmåten til <guimenuitem>Ikoner</guimenuitem> (<keycombo action=\"simul\">&Ctrl;<keycap>1</keycap></keycombo>), <guimenuitem>Detaljer</guimenuitem> (<keycombo action=\"simul\">&Ctrl;<keycap>2</keycap></keycombo>) eller <guimenuitem>Kolonner</guimenuitem> (<keycombo action=\"simul\">&Ctrl;<keycap>3</keycap></keycombo>."

#~ msgid "2009-11-18"
#~ msgstr "2009-11-18"

#~ msgid "<releaseinfo>4.4</releaseinfo>"
#~ msgstr "<releaseinfo>4.4</releaseinfo>"

#~ msgid "KControl"
#~ msgstr "KControl"

#~ msgid "Appearance Tab"
#~ msgstr "Utseende"

#~ msgid "Here you can configure how icons on your desktop appear."
#~ msgstr "Her kan du bestemme hvordan ikonene på skrivebordet skal se ut."

#~ msgid "Standard font:"
#~ msgstr "Standard skrifttype:"

#~ msgid "This option can be used to change the typeface used on the desktop. Simply select your typeface from the dropdown box."
#~ msgstr "Dette kan brukes til å endre skrifta som brukes på skrivebordet. Velg skrift fra rulleboksen."

#~ msgid "Font size:"
#~ msgstr "Skriftstørrelse:"

#~ msgid "You can change the relative size of the text on the Desktop."
#~ msgstr "Du kan endre den relative størrelsen på teksten på skrivebordet."

#~ msgid "Normal text color:"
#~ msgstr "Farge på vanlig tekst:"

#~ msgid "This option lets you select the color of normal (or unhighlighted) text."
#~ msgstr "Med denne knappen kan du velge farge for vanlig (ikke uthevet) tekst."

#~ msgid "Text background color:"
#~ msgstr "Bakgrunnsfarge for tekst:"

#~ msgid "This option lets you select the background color of normal text. If left unchecked, the text has a transparent background. If selected, you can choose the color by pressing the button."
#~ msgstr "Her kan du velge bakgrunnsfarge for vanlig tekst. Hvs du ikke setter kryss får teksten gjennomsiktig bakgrunn. Hvis du krysser av kan du velge bakgrunnsfarge med knappen."

#~ msgid "Underline filenames:"
#~ msgstr "Understrek filnavn:"

#~ msgid "Determines if file names are underlined on the Desktop."
#~ msgstr "Bestemmer om filnavn skal understrekes på skrivebordet."

#~ msgid "Desktop"
#~ msgstr "Skrivebord"

#~ msgid "&kde; offers you the possibility to have several virtual desktops. In this tab you can configure the number of desktops as well as their names. Just use the slider to adjust the number of desktops. You can assign names to the desktops by entering text into the text fields below."
#~ msgstr "&kde; gir deg mulighet til å ha flere virtuelle skrivebord. På dette bladet kan du bestemme antall skrivebord og hva de skal hete. Bare bruk skyveren til å justere antallet skrivebord. Du kan tilordne navn til skrivebordene ved å oppgi tekst i tekstrutene nedenfor."

import os,sys
import Lokalize

for p in sys.path:
    if os.path.exists(p+'/../scripts-trunk/lokalize') and not p.endswith('/scripts-trunk/lokalize'):
        sys.path=sys.path+[p+'/../scripts-trunk/lokalize']

try:
    import multitarget
except:
    print "svn up ../scripts-trunk OR fix search path?"


observer=multitarget.MultiTarget(Lokalize,'/nb/','/nn/')
Lokalize.connect('editorActivated()',observer.editorActivated)
